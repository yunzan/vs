<?php

namespace Media\VideoBundle\Models;


class Video {
    const STATUS_PUBLISHED = 1;
    const STATUS_UNPUBLISHED = 0;
}
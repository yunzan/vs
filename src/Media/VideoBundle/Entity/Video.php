<?php

namespace Media\VideoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Video
 *
 * @ORM\Table(name="media__video")
 * @ORM\Entity(repositoryClass="Media\VideoBundle\Entity\VideoRepository")
 */
class Video
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="number", type="string", unique=true, length=20)
     */
    private $number;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=60)
     */
    private $title;

    /**
     * @var string
     *
     * @ORM\Column(name="format", type="string", length=20)
     */
    private $format;

    /**
     * @var integer
     *
     * @ORM\Column(name="duration", type="integer")
     */
    private $duration;

    /**
     * @var string
     *
     * @ORM\Column(name="cover", type="string", length=255)
     */
    private $cover;

    /**
     * @var string
     *
     * @ORM\Column(name="source", type="string", length=255)
     */
    private $source;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     */

    private $description;

    /**
     * @var string
     *
     * @ORM\Column(name="speaker", type="string", length=12)
     */
    private $speaker;

    /**
     * @var string
     *
     * @ORM\Column(name="speakerTitle", type="string", length=12)
     */
    private $speakerTitle;

    /**
     * @var string
     *
     * @ORM\Column(name="speakerAvatar", type="string", length=60, nullable=true)
     */
    private $speakerAvatar;

    /**
     * @var string
     *
     * @ORM\Column(name="speakerIntro", type="string", length=225, nullable=true))
     */
    private $speakerIntro;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * @var integer
     *
     * @ORM\Column(name="size", type="integer", nullable=true))
     */
    private $size;

    /**
     * @var boolean
     *
     * @ORM\Column(name="allowSale", type="boolean")
     */
    private $allowSale = true;

    /**
     * @var string
     *
     * @ORM\Column(name="slug", type="string", nullable=true)
     */
    private $slug;

    /**
     * @return string
     */
    public function getSpeakerAvatar()
    {
        return $this->speakerAvatar;
    }

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status = 1;

    /**
     * @var integer
     *
     * @ORM\Column(name="position", type="integer")
     */
    private $position = 0;

    /**
     * Set position
     *
     * @param integer $position
     * @return Video
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * @var boolean
     *
     * @ORM\Column(name="onlyInCourse", type="boolean")
     */
    private $onlyInCourse = false;

    /**
     * @return boolean
     */
    public function isOnlyInCourse()
    {
        return $this->onlyInCourse;
    }

    /**
     * @return boolean
     */
    public function getOnlyInCourse()
    {
        return $this->onlyInCourse;
    }

    /**
     * @param boolean $onlyInCourse
     */
    public function setOnlyInCourse($onlyInCourse)
    {
        $this->onlyInCourse = $onlyInCourse;
    }

    /**
     * @param string $speakerAvatar
     */
    public function setSpeakerAvatar($speakerAvatar)
    {
        $this->speakerAvatar = $speakerAvatar;
    }

    /**
     * @return string
     */
    public function getSpeakerIntro()
    {
        return $this->speakerIntro;
    }

    /**
     * @param string $speakerIntro
     */
    public function setSpeakerIntro($speakerIntro)
    {
        $this->speakerIntro = $speakerIntro;
    }

    /**
     * @return string
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param string $slug
     */
    public function setSlug($slug)
    {
        $this->slug = $slug;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set number
     *
     * @param string $number
     * @return Video
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Video
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set format
     *
     * @param string $format
     * @return Video
     */
    public function setFormat($format)
    {
        $this->format = $format;

        return $this;
    }

    /**
     * Get format
     *
     * @return string 
     */
    public function getFormat()
    {
        return $this->format;
    }

    /**
     * Set duration
     *
     * @param integer $duration
     * @return Video
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return integer 
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set cover
     *
     * @param string $cover
     * @return Video
     */
    public function setCover($cover)
    {
        $this->cover = $cover;

        return $this;
    }

    /**
     * Get cover
     *
     * @return string 
     */
    public function getCover()
    {
        return $this->cover;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Video
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set speaker
     *
     * @param string $speaker
     * @return Video
     */
    public function setSpeaker($speaker)
    {
        $this->speaker = $speaker;

        return $this;
    }

    /**
     * Get speaker
     *
     * @return string 
     */
    public function getSpeaker()
    {
        return $this->speaker;
    }

    /**
     * Set speakerTitle
     *
     * @param string $speakerTitle
     * @return Video
     */
    public function setSpeakerTitle($speakerTitle)
    {
        $this->speakerTitle = $speakerTitle;

        return $this;
    }

    /**
     * Get speakerTitle
     *
     * @return string 
     */
    public function getSpeakerTitle()
    {
        return $this->speakerTitle;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return Video
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * @ORM\PrePersist
     */
    public function setCreatedValue()
    {
        $this->created = new \DateTime();
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set size
     *
     * @param integer $size
     * @return Video
     */
    public function setSize($size)
    {
        $this->size = $size;

        return $this;
    }

    /**
     * Get size
     *
     * @return integer 
     */
    public function getSize()
    {
        return $this->size;
    }

    /**
     * Set allowSale
     *
     * @param boolean $allowSale
     * @return Video
     */
    public function setAllowSale($allowSale)
    {
        $this->allowSale = $allowSale;

        return $this;
    }

    /**
     * Get allowSale
     *
     * @return boolean 
     */
    public function getAllowSale()
    {
        return $this->allowSale;
    }


    /**
     * Set source
     *
     * @param string $source
     * @return Video
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source
     *
     * @return string 
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return Video
     */
    public function setStatus($status)
    {
        $this->status = $status;
        return $this;
    }
}

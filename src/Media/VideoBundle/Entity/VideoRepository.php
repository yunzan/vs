<?php

namespace Media\VideoBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Media\VideoBundle\Models\Video as V;

class VideoRepository extends EntityRepository
{
    const RESULT_MIN = 10;
    public function searchResult($word = '')
    {
        $query = $this->getEntityManager()
          ->createQuery(
            'SELECT v FROM MediaVideoBundle:Video v
              WHERE v.title LIKE :word
              AND v.status = :status
              AND v.onlyInCourse <> 1'
          )
          ->setMaxResults(self::RESULT_MIN)
          ->setParameter('word', "%$word%")
          ->setParameter('status', V::STATUS_PUBLISHED);
        try {
            $res = $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            $res = null;
        }

        $less = self::RESULT_MIN - count($res);

        if($less) {
            $query = $this->getEntityManager()
              ->createQuery(
                'SELECT v FROM MediaVideoBundle:Video v
              WHERE v.description LIKE :word'
              )
              ->setMaxResults($less)
              ->setParameter('word', "%$word%");
            try {
                $res += $query->getResult();
            } catch (\Doctrine\ORM\NoResultException $e) {
                return $res;
            }
        }

        return $res;
    }

    /**
     * 首页随机视频（推荐视频）
     */
    public function randomVideos()
    {
        $query = $this->getEntityManager()
            ->createQuery(
                'SELECT v FROM MediaVideoBundle:Video v
              WHERE v.status = :status
              AND v.onlyInCourse <> 1'
            )
            ->setFirstResult(rand(0,500))
            ->setMaxResults(500)
            ->setParameter('status', V::STATUS_PUBLISHED);
        try {
            $res = $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            $res = null;
        }

        shuffle($res);
        $new = [];
        for($i = 0; $i < 12; $i++) {
            $new[] = $res[$i];
        }

        return $new;
    }

    public function findVideosByTerm($term)
    {
        $query = $this->getEntityManager()
            ->createQuery(
                'SELECT v FROM MediaVideoBundle:Video v
            LEFT JOIN MediaVideoBundle:VideoTerm vt
            WHERE vt.video = v.id
            WHERE v.status = :status AND
            vt.term = :term'
            )
            ->setMaxResults(2)
            ->setParameter('status', V::STATUS_PUBLISHED)
            ->setParameter('term', $term->getId());
        try {
            //echo $query->getSQL(); exit;
            $res = $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            $res = null;
        }

        return $res;
    }
}
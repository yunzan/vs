<?php

namespace Media\VideoBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VideoTerm
 *
 * @ORM\Table(name="media__video_term")
 * @ORM\Entity
 */
class VideoTerm
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\JoinColumn(name="video_id", referencedColumnName="id", unique=true)
     * @ORM\ManyToOne(targetEntity="Media\VideoBundle\Entity\Video")
     */
    private $video;

    /**
     * @ORM\JoinColumn(name="term_id", referencedColumnName="id")
     * @ORM\ManyToOne(targetEntity="App\CategoryBundle\Entity\Term")
     */
    private $term;



    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set video
     *
     * @param \Media\VideoBundle\Entity\Video $video
     * @return VideoTerm
     */
    public function setVideo(\Media\VideoBundle\Entity\Video $video = null)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get video
     *
     * @return \Media\VideoBundle\Entity\Video 
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Set term
     *
     * @param \App\CategoryBundle\Entity\Term $term
     * @return VideoTerm
     */
    public function setTerm(\App\CategoryBundle\Entity\Term $term = null)
    {
        $this->term = $term;

        return $this;
    }

    /**
     * Get term
     *
     * @return \App\CategoryBundle\Entity\Term 
     */
    public function getTerm()
    {
        return $this->term;
    }
}

<?php

namespace App\MobileBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Media\VideoBundle\Models\Video as V;
/**
 * Class MemberController
 * @Route("/", host="app.veishu.com")
 */
class HomeController extends Controller
{
    /**
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        $promote_videos = $this->get('request')->getSession()->get('promote_videos');
        if(!$promote_videos) {
            $promote_videos = $em->getRepository('MediaVideoBundle:Video')->randomVideos();
            $this->get('request')->getSession()->set('promote_videos', $promote_videos);
        }

        $categories = $em->getRepository('AppCategoryBundle:Category')->findBy(array(), array('position' => 'ASC'));
        $courses = $em->getRepository('AppWebBundle:Course')->findAll();

        return array(
          'promote_videos' => $promote_videos,
          'categories' => $categories,
          'courses' => $courses,
        );
    }

    /**
     * @Route("/video/{slug}.html")
     * @Template()
     */
    public function playAction($slug)
    {
        $em = $this->getDoctrine()->getManager();
        $condition = array('slug' => $slug);
        if(is_numeric($slug)) {
            $condition = array('number' => $slug, 'status' => V::STATUS_PUBLISHED);
        }
        $video = $em->getRepository('MediaVideoBundle:Video')->findOneBy($condition);
        return array(
          'video' => $video,
          'back_url' => $this->get('request')->get('back_url')
        );
    }

    /**
     * @Route("/videos/category/{slug}.html")
     * @Template()
     */
    public function videosAction($slug)
    {
        $em = $this->getDoctrine()->getManager();
        $scroll = intval($this->get('request')->get('page'));
        $per_page = $this->get('request')->get('per_page')?intval($this->get('request')->get('per_page')):10;
        $category = $em->getRepository('AppCategoryBundle:Category')->findOneBy(array('slug' => $slug));
        $categories = $em->getRepository('AppCategoryBundle:Category')->findBy(array(), array('position' => 'ASC'));
        if(!$category) {
            throw $this->createNotFoundException();
        }

        $videos = $em->getRepository('AppCategoryBundle:Category')->findAllVideoByCategory(
            $category,
            $per_page,
            $scroll * $per_page
        );

        return array(
            'videos' => $videos,
            'categories' => $categories,
            'title'  => $category->getName(),
            'back_url' => $this->get('request')->get('back_url')?$this->get('request')->get('back_url'):$this->generateUrl('app_mobile_home_index',[],true),
            'current_url' => $this->generateUrl('app_mobile_home_videos',array('slug'=>$slug),true)
        );
    }

    /**
     * @Route("/courses.html")
     * @Template()
     */
    public function coursesAction()
    {
        return array(
                // ...
            );
    }

    /**
     * @Route("/course/{slug}.html")
     * @Template()
     */
    public function courseAction($slug)
    {
        $em = $this->getDoctrine()->getManager();
        $course = $em->getRepository('AppWebBundle:Course')->findOneBySlug($slug);
        $chapters = $em->getRepository('AppWebBundle:Course')->getChapters($course);

        return array(
            'course' => $course,
            'chapters' => $chapters,
            'current_url' => $this->generateUrl('app_mobile_home_course',array('slug'=>$slug),true),
            'back_url' => $this->get('request')->get('back_url')?$this->get('request')->get('back_url'):$this->generateUrl('app_mobile_home_index',[],true),
        );
    }

}

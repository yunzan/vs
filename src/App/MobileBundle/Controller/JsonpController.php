<?php

namespace App\MobileBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Media\VideoBundle\Models\Video as V;

/**
 * Class MemberController
 * @Route("/jsonp", host="app.veishu.com")
 */
class JsonpController extends Controller
{
    /**
     * Course details
     *
     * @Route("/course/{slug}.js")
     *
     */
    public function courseAction($slug)
    {
        $data = [];
        $em = $this->getDoctrine()->getManager();
        $callback = $this->get('request')->get('callback');
        try{
            $course = $em->getRepository('AppWebBundle:Course')->findOneBySlug($slug);
            if($course) {
                $data['title'] = $course->getTitle();
                $data['cover'] = $course->getCover();
                $data['description'] = $course->getDescription();
                $data['slug'] = $course->getSlug();
                $data['position'] = $course->getPosition();
                $data['chapters'] = $this->_getChapters($course);
            }
            $response = json_encode(array(
                'success' => true,
                'data' => $data
            ));
        } catch(Exception $e){
            $response = json_encode(array(
                'success' => false,
                'message' => $e
            ));
        }

        if($callback){
            $response = $callback . '(' .$response .');';
        }

        echo $response;
        exit;
    }

    /**
     * @Route("/courses/{type}.js", defaults={"type": "all"}, requirements={"type": "all"})
     *
     */
    public function coursesAction($type)
    {
        $data = [];
        $em = $this->getDoctrine()->getManager();
        $callback = $this->get('request')->get('callback');
        try{
            $courses = $em->getRepository('AppWebBundle:Course')->findAll();
            foreach($courses as $course) {
                $dc = [];
                $dc['title'] = $course->getTitle();
                $dc['cover'] = $course->getCover();
                $dc['description'] = $course->getDescription();
                $dc['slug'] = $course->getSlug();
                $dc['position'] = $course->getPosition();
                $data[] = $dc;
            }
            $response = json_encode(array(
                'success' => true,
                'data' => $data
            ));
        } catch(Exception $e){
            $response = json_encode(array(
                'success' => false,
                'message' => $e
            ));
        }

        if($callback){
            $response = $callback . '(' .$response .');';
        }

        echo $response;
        exit;
    }

    /**
     * @Route("/videos/{type}/{value}.js", defaults={"type": "default", "value": "all"}, requirements={"type": "default|category|promote"})
     *
     */
    public function videosAction($type, $value)
    {
        $data = [];
        $em = $this->getDoctrine()->getManager();
        $callback = $this->get('request')->get('callback');
        try{
            switch($type) {
                case 'category':
                    $scroll = intval($this->get('request')->get('page'));
                    $per_page = $this->get('request')->get('per_page')?intval($this->get('request')->get('per_page')):10;
                    $category = $em->getRepository('AppCategoryBundle:Category')->findOneBy(array('slug' => $value));
                    if(!$category) {
                        throw $this->createNotFoundException();
                    }

                    $videos = $em->getRepository('AppCategoryBundle:Category')->findAllVideoByCategory(
                        $category,
                        $per_page,
                        $scroll * $per_page
                    );
                    $data = $this->_wrapVideoData($videos);
                    break;
                case 'promote':
                    $random_videos = $em->getRepository('MediaVideoBundle:Video')->randomVideos();
                    $data = $this->_wrapVideoData($random_videos);
                    break;
                case 'default':
                    $scroll = intval($this->get('request')->get('page'));
                    $per_page = $this->get('request')->get('per_page')?intval($this->get('request')->get('per_page')):10;
                    $order = $this->get('request')->get('order');
                    $em = $this->getDoctrine()->getManager();

                    $videos = $em->getRepository('MediaVideoBundle:Video')->findBy(
                        array('status' => 1, 'onlyInCourse' => 0),
                        array('position' => 'ASC'),
                        $per_page,
                        $scroll * $per_page
                    );
                    $data = $this->_wrapVideoData($videos);
                    break;
            }
            $response = json_encode(array(
                'success' => true,
                'data' => $data,
            ));
        } catch(Exception $e) {
            $response = json_encode(array(
                'success' => false,
                'message' => $e,
            ));
        }

        if($callback){
            $response = $callback . '(' .$response .');';
        }

        echo $response;
        exit;
    }

    /**
     * @Route("/categories/{type}.js", defaults={"type": "all"}, requirements={"type": "all"})
     *
     */
    public function categoriesAction($type)
    {
        $data = [];
        $em = $this->getDoctrine()->getManager();
        $callback = $this->get('request')->get('callback');
        try{
            $categories = $em->getRepository('AppCategoryBundle:Category')->findBy(array(), array('position' => 'ASC'));

            foreach($categories as $cate){
                $cd = [];
                $cd['name'] = $cate->getName();
                $cd['position'] = $cate->getPosition();
                $cd['slug'] = $cate->getSlug();
                $data[] = $cd;
            }

            $response = json_encode(array(
                'success' => true,
                'data' => $data,
            ));
        } catch(Exception $e){
            $response = json_encode(array(
                'success' => false,
                'message' => $e,
            ));
        }

        if($callback){
            $response = $callback . '(' .$response .');';
        }

        echo $response;
        exit;
    }

    /**
     * @Route("/video/{slug}.js")
     *
     */
    public function videoAction($slug)
    {
        $em = $this->getDoctrine()->getManager();
        $condition = array('slug' => $slug);
        $callback = $this->get('request')->get('callback');

        if(is_numeric($slug)) {
            $condition = array('number' => $slug, 'status' => V::STATUS_PUBLISHED);
        }
        try{
            $video = $em->getRepository('MediaVideoBundle:Video')->findOneBy($condition);
            $snapshots = $em->getRepository('MediaVideoBundle:Snapshot')->findByVideo($video, array('point' => 'ASC'));
            $vd = $this->_wrapVideoData([$video])[0];
            foreach($snapshots as $sn){
                $sd = [];
                $sd['point'] = $sn->getPoint();
                $sd['summary'] = $sn->getSummary();
                $vd['snapshots'][] = $sd;
            }
            $response = json_encode(array(
                'success' => true,
                'data' => $vd
            ));
        }catch (Exception $e){
            $response = json_encode(array(
                'success' => false,
                'message' => $e
            ));
        }
        if($callback){
            $response = $callback . '(' .$response .');';
        }

        echo $response;
        exit;
    }


    protected function _wrapVideoData($videos)
    {
        $data = [];
        foreach($videos as $video){
            $vd = [];
            $vd['number'] = $video->getNumber();
            $vd['title'] = $video->getTitle();
            $vd['cover'] = $video->getCover();
            $vd['source'] = $video->getSource();
            $vd['duration'] = $video->getDuration();
            $vd['format'] = $video->getFormat();
            $vd['description'] = $video->getDescription();
            $vd['speaker'] = $video->getSpeaker();
            $vd['speakerTitle'] = $video->getSpeakerTitle();
            $vd['speakerAvatar'] = $video->getSpeakerAvatar();
            $vd['speakerIntro'] = $video->getSpeakerIntro();
            $vd['created'] = date('Y-m-d', $video->getCreated()->getTimestamp());
            $data[] = $vd;
        }

        return $data;
    }

    protected function _getChapters($course)
    {
        $chapters = [];
        $em = $this->getDoctrine()->getManager();
        $cr = $em->getRepository('AppWebBundle:Course');
        $temp = $cr->getChapters($course);
        foreach($temp as $k => $ch){
            $chp = [];
            $chp['title'] = $k;
            $chp['videos'] = $this->_wrapVideoData($ch['videos']);
            $chapters[] = $chp;
        }

        return $chapters;
    }

}

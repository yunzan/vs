<?php

namespace App\MobileBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class WechatController
 * @package App\MobileBundle\Controller
 * @Route("/", host="m.weshu.cn")
 */
class WechatController extends Controller
{
    const PER_PAGE = 15;
    /**
     * @Route("/c/{id}", name="wx_cate_list")
     * @Template()
     */
    public function listAction($id)
    {
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('AppCategoryBundle:Category')->find($id);
        $res = $em->getRepository('AppCategoryBundle:Term')->findByCategory($category);

        foreach($res as $term) {
            $term->videos = $em->getRepository('MediaVideoBundle:Video')->findVideosByTerm($term);
            $terms[] = $term;
        }

        return array(
            'terms' => $terms,
            'cid'   => $id,
            'title' => $category->getName(),
        );
    }

    /**
     * @Route("/c/{cid}/t/{tid}", name="wx_term_list")
     * @Template()
     */
    public function list2Action($cid, $tid)
    {
        $query = array(); $videos = array();
        $scroll = intval($this->get('request')->get('scroll'));
        $order = $this->get('request')->get('order');
        $em = $this->getDoctrine()->getManager();

        $category = $em->getRepository('AppCategoryBundle:Category')->find($cid);
        $term = $em->getRepository('AppCategoryBundle:Term')->find($tid);
        if(!$category || !$term) {
            throw $this->createNotFoundException();
        }

        $videos = $em->getRepository('AppCategoryBundle:Category')->findAllVideoByTerm($term, self::PER_PAGE, $scroll * self::PER_PAGE);
        $pages = ceil($em->getRepository('AppCategoryBundle:Category')->countVideoByTerm($term)/self::PER_PAGE);


        if($order) {
            $query = array(
                'order' => $order,
            );
        }
        if($scroll) {
            if(empty($videos)) {
                throw $this->createNotFoundException();
            }
            return $this->render('AppMobileBundle:Wechat:scroll.html.twig', array(
                'videos' => $videos,
            ));
        }
        return array(
            'videos' => $videos,
            'title'  => $term->getName(),
            'pages'  => $pages,
            'cid' => $cid,
            'tid' => $tid,
        );
    }

    /**
     * @Route("/v/{id}", name="wx_video")
     * @Template()
     */
    public function videoAction($id)
    {
        return array(
                // ...
            );
    }

    /**
     * @Route("/about")
     * @Template()
     */
    public function aboutAction()
    {
        return array(
                // ...
            );    }

}

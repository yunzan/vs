<?php

namespace App\MobileBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class HomeControllerTest extends WebTestCase
{
    public function testIndex()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/');
    }

    public function testPlay()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/video/{slug}.html');
    }

    public function testVideos()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/videos/category/{slug}.html');
    }

    public function testCourses()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/courses.html');
    }

    public function testCourse()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/course/{slug}.html');
    }

}

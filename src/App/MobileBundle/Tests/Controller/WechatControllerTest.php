<?php

namespace App\MobileBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class WechatControllerTest extends WebTestCase
{
    public function testList()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/c/{slug}');
    }

    public function testList2()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/c/{slug}/t/{slug}');
    }

    public function testVideo()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/video/{id}');
    }

    public function testAbout()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/about');
    }

}

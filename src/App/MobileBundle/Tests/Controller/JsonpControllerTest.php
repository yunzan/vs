<?php

namespace App\MobileBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class JsonpControllerTest extends WebTestCase
{
    public function testCourse()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/course');
    }

    public function testCourses()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/courses.js');
    }

    public function testVideos()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/videos.js');
    }

    public function testCategories()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', 'categories.js');
    }

    public function testVideo()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/video.js');
    }

}

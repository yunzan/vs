/**
 * Created by Jeremy on 4/26/2015.
 */
Registry = {};
Registry.img_host = 'http://img.veishu.com/';
Registry.video_host = 'http://v.veishu.com/';
Registry.api_host = 'http://app.veishu.com';
Registry.api_prefix = '/jsonp/';
Registry.courses_url = 'courses/all.js';
Registry.videos_url = 'videos/default/all.js';
Registry.categories_url = 'categories/all.js';
Registry.videos_promote_url = 'videos/promote/all.js';
Registry.videos_cate_url = 'videos/category/{slug}.js';
Registry.course_details_url = 'course/{slug}.js';
Registry.video_details_url = 'video/{slug}.js';
var api_url = Registry.api_host + Registry.api_prefix;
$(document).ready(function(){
    //获取推荐视频
    $.ajax({
        type: "get",
        async: false,
        url: api_url + Registry.videos_promote_url,
        dataType: "jsonp",
        success: function(json){
            Registry.videos_promote = json.data;
            var items = renderVideos(json.data);
            $(items).appendTo($('#videos_promote'));
            bindPlay();
        },
        error: function(){
            console.log('error');
        }
    });

    $.ajax({
        type: "get",
        async: false,
        url: api_url + Registry.courses_url,
        dataType: "jsonp",
        success: function(json){
            Registry.courses = json.data;
            var items = renderCourses(json.data);
            $(items).appendTo($('#course_list'));
        },
        error: function(){
            console.log('error');
        }
    });

    $.ajax({
        type: "get",
        async: false,
        url: api_url + Registry.categories_url,
        dataType: "jsonp",
        success: function(json){
            Registry.courses = json.data;
            var items = renderCategories(json.data);
            $(items).appendTo($('#category_list'));
        },
        error: function(){
            console.log('error');
        }
    });
});

var renderVideos = function(videos){
    var output = '';
    $.each(videos, function(idx,video){
        output +=  '<li class="table-view-cell media">';
        output +=  '    <a href="#playModal" class="navigate-right play" video-number="'+video.number+'">';
        output +=  '        <img width="96" class="media-object pull-left" src="'+ Registry.img_host + video.cover +'" >';
        output +=  '        <div class="media-body">';
        output +=  '        ' + video.title;
        output +=  '        <p>' + video.description + '<p>';
        output +=  '        </div>';
        output +=  '    </a>';
        output +=  '</li>';
    });

    return output;
}

var renderCourses = function(courses){
    var output = '';
    $.each(courses, function(idx,course){
        output += '<li class="table-view-cell media">';
        output +=  '    <a class="navigate-right">';
        output +=  '        <img width="100%" class="media-object pull-left" src="'+ Registry.img_host + course.cover +'" >';
        output +=  '        <div>';
        output +=  '        ' + course.title;
        output +=  '        </div>';
        output +=  '    </a>';
        output +=  '</li>';
    });

    return output;
}

var renderCategories = function(categories){
    var output = '';
    $.each(categories, function(idx,cate){
        output += '<li class="table-view-cell media">';
        output +=  '    <a class="navigate-right" data-transition="slide-in">';
        output +=  '        <div>';
        output +=  '        ' + cate.name;
        output +=  '        </div>';
        output +=  '    </a>';
        output +=  '</li>';
    });

    return output;
}

var bindPlay = function(){
    $('.media a.play').on('click', function(){
        var self = $(this);
        var _number = self.attr('video-number');
        var reg = new RegExp('\{slug\}','g');
        var vurl = Registry.video_details_url.replace(reg, _number);
        $.ajax({
            type: "get",
            async: false,
            url: api_url + vurl,
            dataType: "jsonp",
            success: function(json){
                var video = json.data;
                //$('#video_play source').attr('src', Registry.video_host + video.source);
                Registry.v = videojs('video_play');
                Registry.v.src(Registry.video_host + video.source);
                console.log(Registry.video_host + video.source);
                Registry.v.play();
            },
            error: function(){
                console.log('error');
            }
        });
    });

    $('a#close_play').on('click', function(){
        Registry.v.pause();
    });
}
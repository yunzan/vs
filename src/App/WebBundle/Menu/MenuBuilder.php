<?php

namespace App\WebBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContextInterface;

class MenuBuilder
{

    private $factory;

    private $securityContext;

    /**
     * @param FactoryInterface $factory
     */
    public function __construct(FactoryInterface $factory, SecurityContextInterface $securityContext)
    {
        $this->factory = $factory;
        $this->securityContext = $securityContext;
    }

    public function createMainMenu(Request $request, $doctrine)
    {
        $em = $doctrine->getManager();
        $category = $em->getRepository('AppCategoryBundle:Category')->findOneBy(array(), array('position' => 'ASC'));
        $menu = $this->factory->createItem('root');

        //$menu->addChild('视频', array('route' => 'video',));
        $menu->addChild('视频', array(
            'route' => 'video_list',
            'routeParameters' => array('slug' => $category->getSlug()),
        ));
        $menu->addChild('课程', array('route' => 'course',));
        if($this->securityContext->isGranted('ROLE_USER')) {
            $menu->addChild('轨迹', array('route' => 'member_course',));
        }

        $slugs = explode('_', $request->get('_route'));

        switch($slugs[0]) {
            case 'member':
                $menu['轨迹']->setCurrent(true);
                break;
            case 'course':
                $menu['课程']->setCurrent(true);
                break;
            case 'video':
                $menu['视频']->setCurrent(true);
                break;
            default:
                break;
        }

        return $menu;
    }

    public function createMemberSidebarMenu(Request $request)
    {
        $menu = $this->factory->createItem('memberSidebar', array(
          'childrenAttributes' => array(
            'class' => 'treenav',
          ),
        ));

        $menu->addChild('我的课程', array('route' => 'member_course',));
        $menu->addChild('我的视频', array('route' => 'member_video',));
        $menu->addChild('我的经验', array('route' => 'member_experience',));
        $menu->addChild('个人设置', array('route' => 'member_settings',));
        $menu->addChild('我的消息', array('route' => 'member_message',));

        $slugs = explode('_', $request->get('_route'));

        switch($slugs[1]) {
            case 'course':
                $menu['我的课程']->setCurrent(true);
                break;
            case 'video':
                $menu['我的视频']->setCurrent(true);
                break;
            case 'settings':
                $menu['个人设置']->setCurrent(true);
                break;
            case 'experience':
                $menu['我的经验']->setCurrent(true);
                break;
            case 'message':
                $menu['我的消息']->setCurrent(true);
                break;
            default:
                break;
        }

        return $menu;
    }

    public function createCategorySidebarMenu(Request $request, $doctrine)
    {
        $em = $doctrine->getManager();
        $categories = $em->getRepository('AppCategoryBundle:Category')->findBy(array(), array('position' => 'ASC'));

        $menu = $this->factory->createItem('categorySidebar', array(
          'childrenAttributes' => array(
            'class' => 'treenav',
          ),
        ));

        $menu->addChild('全部', array('route' => 'video',));

        foreach($categories as $category) {
            $menu->addChild($category->getName(), array(
              'route' => 'video_list',
              'routeParameters' => array('slug' => $category->getSlug()),
            ));
        }

        return $menu;
    }

    public function createMemberCourseTab(Request $request)
    {
        $menu = $this->factory->createItem('memberCourse', array(
          'childrenAttributes' => array(
            'class' => 'classify',
          ),
        ));

        $menu->addChild('在学', array('route' => 'member_course',));
        $menu->addChild('学完', array(
          'route' => 'member_course_alias',
          'routeParameters' => array('tag' => 'learned'),
        ));
        $menu->addChild('收藏', array(
          'route' => 'member_course_alias',
          'routeParameters' => array('tag' => 'collection'),
        ));

        return $menu;
    }

    public function createMemberVideoTab(Request $request)
    {
        $menu = $this->factory->createItem('memberVideo', array(
          'childrenAttributes' => array(
            'class' => 'classify',
          ),
        ));

        $menu->addChild('播放记录', array('route' => 'member_video',));
        $menu->addChild('收藏', array(
          'route' => 'member_video_alias',
          'routeParameters' => array('tag' => 'collection'),
        ));

        return $menu;
    }

    public function createMemberSettingsTab(Request $request)
    {
        $menu = $this->factory->createItem('memberSetting', array(
          'childrenAttributes' => array(
            'class' => 'classify',
          ),
        ));

        $menu->addChild('资料设置', array('route' => 'member_settings',));
        $menu->addChild('账号设置', array(
          'route' => 'member_settings_account',
        ));
        $menu->addChild('隐私设置', array(
          'route' => 'member_settings_privacy',
        ));
        $menu->addChild('通知设置', array(
          'route' => 'member_settings_notification',
        ));
        $menu->addChild('邮件设置', array(
          'route' => 'member_settings_mail',
        ));

        return $menu;
    }

    public function createVideoListTab(Request $request)
    {
        $menu = $this->factory->createItem('videoList', array(
          'childrenAttributes' => array(
            'class' => 'nav nav-tabs',
          ),
        ));

        $order = $request->get('order');

        $uris = explode('?', $request->getRequestUri());

        $slugs = explode('/',$uris[0]);

        if(isset($slugs[3])) {
            $menu->addChild('全部', array(
              'route' => 'video_list',
              'routeParameters' => array('slug' => $slugs[3]),
            ));
            $menu->addChild('最新', array(
              'route' => 'video_list',
              'routeParameters' => array('slug' => $slugs[3], 'order' => 'created'),
            ));
            $menu->addChild('热门', array(
              'route' => 'video_list',
              'routeParameters' => array('slug' => $slugs[3], 'order' => 'collection'),
            ));
        }
        else {
            $menu->addChild('全部', array(
              'route' => 'video',
            ));
            $menu->addChild('最新', array(
              'route' => 'video',
              'routeParameters' => array('order' => 'created'),
            ));
            $menu->addChild('热门', array(
              'route' => 'video',
              'routeParameters' => array('order' => 'collection'),
            ));
        }

        $menu['全部']->setCurrent(false);

        switch($order) {
            case 'created':
                $menu['最新']->setCurrent(true);
                break;
            case 'collection':
                $menu['热门']->setCurrent(true);
                break;
            default:
                $menu['全部']->setCurrent(true);
                break;
        }



        return $menu;
    }

    public function createCourseListTab(Request $request)
    {
        $menu = $this->factory->createItem(
          'courseList',
          array(
            'childrenAttributes' => array(
              'class' => 'nav nav-tabs',
            ),
          )
        );

        $order = $request->get('order');

        $menu->addChild('全部', array(
          'route' => 'course',
        ));
        $menu->addChild('最新', array(
          'route' => 'course',
          'routeParameters' => array('order' => 'created'),
        ));
        $menu->addChild('热门', array(
          'route' => 'course',
          'routeParameters' => array('order' => 'collection'),
        ));

        $menu['全部']->setCurrent(false);

        switch($order) {
            case 'created':
                $menu['最新']->setCurrent(true);
                break;
            case 'collection':
                $menu['热门']->setCurrent(true);
                break;
            default:
                $menu['全部']->setCurrent(true);
                break;
        }

        return $menu;
    }
}
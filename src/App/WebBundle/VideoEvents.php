<?php
/**
 * Created by PhpStorm.
 * User: Jeremy Hu
 * Date: 4/5/15
 * Time: 10:47 PM
 */

namespace App\WebBundle;


final class VideoEvents
{
    /**
     * defined a event name.
     * this event should be dispatch when user play a video
     */
    const VIDEO_PLAY = 'video.play';
    const VIDEO_COURSE_FINISH = 'video.course.finish';

}
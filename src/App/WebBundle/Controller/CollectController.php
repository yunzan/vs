<?php

namespace App\WebBundle\Controller;

use App\WebBundle\Entity\CourseCollect;
use App\WebBundle\Entity\CourseStatics;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\WebBundle\Entity\VideoStatics;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;
use App\WebBundle\Entity\VideoCollect;

/**
 * Class CollectController
 * @Route("/collect")
 */
class CollectController extends Controller
{
    /**
     * @Route("/add", name="collect_add")
     * @Method("POST")
     */
    public function addAction(Request $request)
    {
        //获取当前用户
        $user = $this->getUser();
        if(!$user) {
            echo 'X-X';
            exit;
        }
        //实体管理器
        $em = $this->getDoctrine()->getManager();
        //vid
        $vid = $request->get('vid');
        //cid
        $cid = $request->get('cid');

        if($cid) {
            $this->collectCourse($cid);
            exit;
        }
        $video = $em->getRepository('MediaVideoBundle:Video')->find($vid);
        if(!$video){
            echo 'X-X';
            die();
        }

        //更新用户的播放记录

        $track = $em->getRepository('AppWebBundle:VideoCollect')->findOneby(array('user' => $user, 'video' => $video));
        if($track) {
            echo '^.^';
            exit;
        }
        $track = new VideoCollect();
        $track->setVideo($video);
        $track->setUser($user);
        $track->setCreated(new \DateTime());
        $em->persist($track);

        //更新视频收藏次数
        $videoStatics = $em->getRepository('AppWebBundle:VideoStatics')->findOneByVideo($video);
        if(!$videoStatics) {
            $videoStatics = new VideoStatics();
        }
        $videoStatics->collectOnce();
        $em->persist($videoStatics);

        $em->flush();
        echo '^.^';
        exit;
    }
    /**
     * @Route("/del", name="collect_del")
     * @Method("DELETE")
     */
    public function delAction(Request $request)
    {
        $user = $this->getUser();
        $vid = $request->get('vid');
        if(!$user || !$vid) {
            echo 'X-X1';
            exit;
        }

        $em = $this->getDoctrine()->getManager();
        $video = $em->getRepository('MediaVideoBundle:Video')->find($vid);
        $vc = $em->getRepository('AppWebBundle:VideoCollect')->findOneBy(array('user'=>$user,'video'=>$video));
        if($vc) {
            $em->remove($vc);
            $em->flush();
            echo '^.^';
        } else {
            echo 'X-X2';
        }
        exit;
    }

    protected function collectCourse($cid)
    {
        $user = $this->getUser();
        $em = $this->getDoctrine()->getManager();
        $course = $em->getRepository('AppWebBundle:Course')->find($cid);
        if(!$course) {
            echo 'X-X';
            exit;
        }

        $cc = $em->getRepository('AppWebBundle:CourseCollect')->findOneBy(array('user'=>$user, 'course'=>$course));
        if($cc) {
            echo '^.^';
            exit;
        }
        $cc = new CourseCollect();
        $cc->setUser($user);
        $cc->setCourse($course);
        $cc->setCreated(new \DateTime());
        $em->persist($cc);
        $em->flush();

        //更新课程收藏次数
        $courseStatics = $em->getRepository('AppWebBundle:CourseStatics')->findOneByCourse($course);
        if(!$courseStatics) {
            $courseStatics = new CourseStatics();
        }
        $courseStatics->collectOnce();
        $courseStatics->setCourse($course);
        $em->persist($courseStatics);

        $em->flush();
        echo '^.^';
        exit;
    }

}
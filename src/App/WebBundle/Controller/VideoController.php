<?php

namespace App\WebBundle\Controller;

use App\AdminBundle\Models\OSSImageOptions;
use App\AdminBundle\Models\OSSVideoOptions;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Media\VideoBundle\Entity\Video;
use Media\VideoBundle\Form\VideoType;
use Media\VideoBundle\Models\Video as V;

/**
 * Video controller.
 *
 * @Route("/video")
 */
class VideoController extends Controller
{
    const PER_PAGE = 12;

    /**
     * Lists all Video entities.
     *
     * @Route("/", name="video")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $query = array(); $videos = array();
        $scroll = intval($this->get('request')->get('scroll'));
        $order = $this->get('request')->get('order');
        $em = $this->getDoctrine()->getManager();

        $videos = $em->getRepository('MediaVideoBundle:Video')->findBy(
          array('status' => 1, 'onlyInCourse' => 0),
          array('position' => 'ASC'),
          self::PER_PAGE,
          $scroll * self::PER_PAGE
        );

        if($order) {
            $query = array(
              'order' => $order,
            );
        }

        if($scroll) {
            if(!$videos) {
                throw $this->createNotFoundException();
            }
            return $this->render('AppWebBundle:Video:scroll.html.twig', array(
              'videos' => $videos,
              'next_url' => $this->generateUrl('video', array_merge(array(
                'scroll' => $scroll+1,
              ), $query)),
            ));
        }

        return array(
          'videos' => $videos,
          'next_url' => $this->generateUrl('video', array_merge(array('scroll' => 1,), $query)),
        );
    }

    /**
     * Finds and displays a Video entity.
     *
     * @Route("/{slug}", name="video_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($slug)
    {
        $em = $this->getDoctrine()->getManager();

        $condition = array('slug' => $slug);
        if(is_numeric($slug)) {
            $condition = array('number' => $slug, 'status' => V::STATUS_PUBLISHED);
        }
        $video = $em->getRepository('MediaVideoBundle:Video')->findOneBy($condition);
        $snapshots = $em->getRepository('MediaVideoBundle:Snapshot')->findByVideo($video, array('point' => 'ASC'));

        if (!$video) {
            throw $this->createNotFoundException('Unable to find Video entity.');
        }


        return array(
          'video'      => $video,
          'snapshots'  => $snapshots,
        );
    }

    /**
     * Show a list of video by category
     *
     * @Route("/c/{slug}", name="video_list")
     * @Method("GET")
     * @Template()
     */
    public function listAction($slug)
    {
        $query = array(); $videos = array();
        $scroll = intval($this->get('request')->get('scroll'));
        $order = $this->get('request')->get('order');
        $em = $this->getDoctrine()->getManager();

        $category = $em->getRepository('AppCategoryBundle:Category')->findOneBy(array('slug' => $slug));
        if(!$category) {
            throw $this->createNotFoundException();
        }

        $videos = $em->getRepository('AppCategoryBundle:Category')->findAllVideoByCategory($category, self::PER_PAGE, $scroll * self::PER_PAGE);

        if($order) {
            $query = array(
              'order' => $order,
            );
        }
        if($scroll) {
            if(empty($videos)) {
                throw $this->createNotFoundException();
            }
            return $this->render('AppWebBundle:Video:scroll.html.twig', array(
              'videos' => $videos,
              'next_url' => $this->generateUrl('video_list', array_merge(array(
                'slug' => $slug,
                'scroll' => $scroll+1,
              ),$query)),
            ));
        }
        return array(
          'videos' => $videos,
          'category' => $category,
          'next_url' => $this->generateUrl('video_list', array_merge(array(
            'slug' => $slug,
            'scroll' => 1,
          ),$query)),
        );

    }
}
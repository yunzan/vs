<?php

namespace App\WebBundle\Controller;

use App\AdminBundle\Models\OSSImageOptions;
use App\OSSBundle\Models\OSSOptions;
use App\OSSBundle\OSSClient;
use App\UserBundle\Entity\Experience;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Request;
use App\WebBundle\Form\Type\UserProfileFormType;
use App\UserBundle\Entity\User;

/**
 * Class MemberController
 * @Route("/member")
 */
class MemberController extends Controller
{
    /**
     * @Route("/", name="member_course")
     * @Route("/course/{tag}", defaults={"tag": "learning"}, requirements={"tag": "learning|learned|collection"}, name="member_course_alias")
     * @Template()
     */
    public function courseAction($tag = 'learning')
    {
        $cxs = array();
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        switch($tag) {
            case 'learning':
                $cxs = $em->getRepository('AppWebBundle:CourseProgress')->learningCourse($user->getId());
                break;
            case 'learned':
                $cxs = $em->getRepository('AppWebBundle:CourseProgress')->learnedCourse($user->getId());
                break;
            case 'collection':
                $cxs = $em->getRepository('AppWebBundle:CourseCollect')->findByUser($user);
                break;
        }
        return array(
            'cxs' => $cxs,
        );
    }

    /**
     * @Route("/video/", name="member_video")
     * @Route("/video/{tag}/", defaults={"tag": "history"}, requirements={"tag": "history|collection"}, name="member_video_alias")
     * @Template()
     */
    public function videoAction($tag = 'history')
    {
        $vxs = array();
        $em = $this->getDoctrine()->getManager();
        $user = $this->getUser();
        $del_url = '';
        switch($tag) {
            case 'history':
                $vxs = $em->getRepository('AppWebBundle:VideoHistory')->findByUser($user);
                $del_url = $this->generateUrl('history_del');
                break;
            case 'collection':
                $vxs = $em->getRepository('AppWebBundle:VideoCollect')->findByUser($user);
                $del_url = $this->generateUrl('collect_del');
                break;
        }


        return array(
            'vxs' => $vxs,
            'del_url' => $del_url,
        );
    }

    /**
     * @Route("/experience/", name="member_experience")
     * @Template()
     */
    public function experienceAction()
    {
        $limit = 10;
        $scroll = $this->get('request')->get('scroll')?$this->get('request')->get('scroll'):0;
        $em = $this->getDoctrine()->getManager();
        $expRep = $em->getRepository('AppUserBundle:Experience');
        $exps = $expRep->listResult($limit, $limit*$scroll);

        if($scroll) {
            return $this->render('AppWebBundle:Member:experienceScroll.html.twig', array(
                'exps' => $exps,
                'next_url' => $this->generateUrl('member_experience', array(
                    'scroll' => $scroll + 1,
                )),
            ));
        }

        return array(
            'points' => $expRep->getTotalPointsByUser($this->getUser()),
            'loser_percent' => $expRep->getLoserPercent($this->getUser()),
            'exps' => $exps,
            'next_url' => $this->generateUrl('member_experience', array(
                'scroll' => 1,
            )),
        );
    }

    /**
     * @Route("/settings/", name="member_settings", methods="POST|GET")
     * @Route("/settings/profile/", name="member_settings_alias")
     * @Template()
     */
    public function settingsAction(Request $request)
    {
        $form = $this->createForm('user_profile');
        $user = $this->get('security.context')->getToken()->getUser();
        $form->setData($user);

        $form->handleRequest($request);
        if($form->isValid()) {
            //check email
            $em = $this->getDoctrine()->getManager();
            $userRep = $em->getRepository('AppUserBundle:User');
            $existEmailUser = $userRep->findOneBy(array('email' => $user->getEmail()));
            if($existEmailUser && $existEmailUser->getId() != $user->getId()) {
                $emailExistError = new FormError('邮箱已被使用！');
                $form->addError($emailExistError);
            }
            //check nickname
            $existNickNameUser = $userRep->findOneBy(array('nickName' => $user->getNickName()));
            if($existNickNameUser && $existNickNameUser->getId() != $user->getId()) {
                $nicknameExistError = new FormError('昵称已被使用！');
                $form->addError($nicknameExistError);
            }
        }

        if($form->isValid()) {
            $em->persist($form->getData());
            $em->flush();
        }

        return array(
            'form' => $form->createView(),
        );
    }

    /**
     * @Route("/upload/avatar/", name="member_upload_avatar", methods="POST")
     *
     */
    public function uploadAvatarAction(Request $request)
    {
        $handle = $request->files->get('file');
        if($handle && $handle->isValid()) {
            $pathName = $handle->getPathName();
            $from = $handle->guessClientExtension();
            $new = $this->_transferAvatarImage($pathName, $from);
            echo $this->_uploadAvatarToOSS($new);
        }
        die;
    }

    /**
     * @Route("/settings/account/", name="member_settings_account")
     * @Template()
     */
    public function settingsAccountAction(Request $request)
    {
        $user = $this->getUser();

        $form = $this->createForm('change_password');
        $form->setData($user);

        $form->handleRequest($request);
        if ($form->isValid()) {
            $user = $form->getData();
            $userManager = $this->get('fos_user.user_manager');
            $userManager->updateUser($user);
        }

        return array(
            'form' => $form->createView()
        );
    }

    /**
     * @Route("/settings/privacy/", name="member_settings_privacy")
     * @Template()
     */
    public function settingsPrivacyAction()
    {
        return array(
        );
    }

    /**
     * @Route("/settings/notification/", name="member_settings_notification")
     * @Template()
     */
    public function settingsNotificationAction()
    {
        return array(
        );
    }

    /**
     * @Route("/settings/mail/", name="member_settings_mail")
     * @Template()
     */
    public function settingsMailAction()
    {
        return array(
        );
    }

    /**
     * @Route("/message/", name="member_message")
     * @Template()
     */
    public function messageAction()
    {
        return array(
        );
    }

    protected function _transferAvatarImage($image, $from)
    {
        $i = null;
        $user = $this->get('security.context')->getToken()->getUser();
        $target = '/tmp/avatar-'.$user->getId().'.jpg';
        switch($from) {
            case 'bmp':
                $i = imagecreatefromwbmp($image);
                imagejpeg($i, $target, 80);
            break;
            case 'png':
                $i = imagecreatefrompng($image);
                imagejpeg($i, $target, 80);
            break;
            case 'gif':
                $i = imagecreatefromgif($image);
                imagejpeg($i, $target, 80);
            break;
            case 'jpeg':
            case 'jpg':
                $i = imagecreatefromjpeg($image);
                imagejpeg($i, $target, 80);
            break;
        }
        if($i) {
            imagedestroy($i);
            unlink($image);
        }
        return $this->_resmapleAvatar($target);
    }

    protected function _resmapleAvatar($image)
    {
        $limit = 200;
        list($ow, $oh) = getimagesize($image);
        if(max($ow, $oh) <= 200) {
            return $image;
        }
        if($ow > $oh) {
            $w = $limit;
            $h = round($oh*($w/$ow));
        }else {
            $h = $limit;
            $w = round($ow*($h/$ow));
        }
        $new = imagecreatetruecolor($limit, $limit);
        imagecopyresampled($new, imagecreatefromjpeg($image), ($limit-$w)/2, ($limit-$h)/2, 0, 0, $w, $h, $ow, $oh);
        imagejpeg($new, $image);
        imagedestroy($new);

        return $image;
    }

    protected function _getAvatarUploadConfig()
    {
        return array(
          OSSOptions::ENDPOINT => $this->container->getParameter(OSSImageOptions::ENDPOINT),
          OSSOptions::ACCESS_KEY_ID => $this->container->getParameter(OSSImageOptions::ACCESS_KEY_ID),
          OSSOptions::ACCESS_KEY_SECRET => $this->container->getParameter(OSSImageOptions::ACCESS_KEY_SECRET),
          OSSOptions::BUCKET => $this->container->getParameter(OSSImageOptions::BUCKET),
        );
    }

    protected function _uploadAvatarToOSS($avatar)
    {
        $user = $this->get('security.context')->getToken()->getUser();
        $fileName = uniqid() . '.jpg';
        $config = $this->_getAvatarUploadConfig();
        $OSS = OSSClient::factory($config);
        $file = new File($avatar);

        $keyAttr = array(
            'avatar',
            $user->getId(),
            $fileName,
        );
        $key = implode(OSSImageOptions::SLASH, $keyAttr);
        $OSS->putObject(array(
          OSSOptions::BUCKET => $config[OSSOptions::BUCKET],
          OSSOptions::KEY => $key,
          OSSOptions::CONTENT => fopen($avatar, 'r'),
          OSSOptions::CONTENT_LENGTH => filesize($avatar),
          OSSOptions::CONTENT_TYPE => $file->getMimeType(),
          OSSOptions::CACHE_CONTROL => OSSImageOptions::CACHE_CONTROL,
          OSSOptions::EXPIRES => new \DateTime(OSSImageOptions::EXPIRE),
          OSSOptions::CONTENT_ENCODING => OSSImageOptions::CONTENT_ENCODING,
          OSSOptions::CONTENT_DISPOSITION => OSSImageOptions::CONTENT_DISPOSITION . 'filename=' . $fileName,
        ));
        unlink($avatar);
        return $key;
    }

}

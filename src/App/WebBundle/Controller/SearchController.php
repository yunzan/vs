<?php

namespace App\WebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * @Route("/search")
 */
class SearchController extends Controller
{
    const SUPPORT_TYPES = 'course,video';
    /**
     * @Route("/", name="app_web_search")
     * @Template()
     */
    public function indexAction()
    {
        $word = $this->get('request')->get('wd');
        $type = $this->get('request')->get('t');
        $supportTypes = explode(',', self::SUPPORT_TYPES);
        $videos = null;
        $courses = null;

        if(!in_array(strtolower($type), $supportTypes)){
            $type = '';
        }

        if($word == '') {
            return $this->redirect($this->generateUrl('app_web_home'));
        }

        $em = $this->getDoctrine()->getManager();

        switch($type) {
            case 'course':
                $courses = $em->getRepository('AppWebBundle:Course')->searchResult($word);
                break;
            case 'video':
                $videos = $em->getRepository('MediaVideoBundle:Video')->searchResult($word);
                break;
            default:
                $courses = $em->getRepository('AppWebBundle:Course')->searchResult($word);
                $videos = $em->getRepository('MediaVideoBundle:Video')->searchResult($word);

                break;
        }


        return array(
            'word' => $word,
            'type' => $type,
            'courses' => $courses,
            'total_c' => count($courses),
            'videos' => $videos,
            'total_v' => count($videos),
            'total' => count($courses) + count($videos),
        );
    }

}

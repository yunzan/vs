<?php

namespace App\WebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class CourseController
 * @Route("/page")
 */
class PageController extends Controller
{
    /**
     * @Route("/about-us", name="page_about_us")
     * @Template()
     */
    public function aboutusAction()
    {
        return array();
    }
    /**
     * @Route("/contact-us", name="page_contact_us")
     * @Template()
     */
    public function contactusAction()
    {
        return array();
    }
    /**
     * @Route("/our-team", name="page_our_team")
     * @Template()
     */
    public function ourteamAction()
    {
        return array();
    }
    /**
     * @Route("/privacy", name="page_privacy")
     * @Template()
     */
    public function privacyAction()
    {
        return array();
    }
    /**
     * @Route("/legal", name="page_legal")
     * @Template()
     */
    public function legalAction()
    {
        return array();
    }
}
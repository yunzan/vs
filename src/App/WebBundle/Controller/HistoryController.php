<?php

namespace App\WebBundle\Controller;

use App\WebBundle\Entity\CourseProgress;
use App\WebBundle\Event\FinishCourseEvent;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use App\WebBundle\Entity\VideoHistory;
use App\WebBundle\Entity\VideoStatics;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints\DateTime;
use App\WebBundle\VideoEvents;
use App\WebBundle\Event\PlayVideoEvent;
use Symfony\Component\EventDispatcher\EventDispatcher;
use App\WebBundle\Listener\VideoPlayListener;

/**
 * Class HistoryController
 * @Route("/history")
 */
class HistoryController extends Controller
{
    /**
     * @Route("/add", name="history_add")
     * @Method("POST")
     */
    public function addAction(Request $request)
    {
        //获取当前用户
        $user = $this->getUser();
        if(!$user) {
            echo 'X-X';
            exit;
        }
        //实体管理器
        $em = $this->getDoctrine()->getManager();
        //vid
        $vid = $request->get('vid');
        //played duration
        $played = $request->get('played');

        $video = $em->getRepository('MediaVideoBundle:Video')->find($vid);
        if(!$video){
            echo 'X-X';
            die();
        }

        $courseVideo = $em->getRepository('AppWebBundle:CourseVideo')->findOneByVideo($video);

        $track = $em->getRepository('AppWebBundle:VideoHistory')->findOneby(array('user' => $user, 'video' => $video));
        if(!$track) {
            $track = new VideoHistory();
            $event = new PlayVideoEvent($video, $user);
            $dispatcher = $this->get('event_dispatcher');
            $dispatcher->dispatch(VideoEvents::VIDEO_PLAY, $event);
        }
        $track->setVideo($video);
        $track->setUser($user);
        if(!$track->getPlayed() || $played > $track->getPlayed()) {
            $track->setPlayed($played);
        }
        $track->setDuration($video->getDuration());
        $track->setUpdated(new \DateTime());
        if(!$track->getCreated()) {
            $track->setCreated(new \DateTime());
        }

        if($courseVideo) {
            $track->setCourse($courseVideo->getCourse());
        }
        $em->persist($track);
        $em->flush();

        //update course progress
        if($courseVideo) {
            $course = $courseVideo->getCourse();
            //course total duration
            $duration = $em->getRepository('AppWebBundle:Course')->getDuration($course);
            //played videos
            $played = $em->getRepository('AppWebBundle:VideoHistory')->coursePlayed($user, $course);
            //calc percent
            $percent = 1;
            if($duration) {
                $percent = ceil($played/$duration * 100);
                if($percent > 100)$percent = 100;
            }
            if($percent == 100) {
                $event = new FinishCourseEvent($course, $user);
                $dispatcher = $this->get('event_dispatcher');
                $dispatcher->dispatch(VideoEvents::VIDEO_COURSE_FINISH, $event);
            }

            $courseProgress = $em->getRepository('AppWebBundle:CourseProgress')->findOneBy(array('course'=>$course, 'user'=> $user));
            if(!$courseProgress) {
                $courseProgress = new CourseProgress();
            }
            $courseProgress->setCourse($course);
            $courseProgress->setUser($user);
            $courseProgress->setPercent($percent);
            $courseProgress->setUpdated(new \DateTime());
            if(!$courseProgress->getCreated()) {
                $courseProgress->setCreated(new \DateTime());
            }
            $em->persist($courseProgress);
            $em->flush();
        }
        echo '^.^';

        exit;
    }

    /**
     * @Route("/del", name="history_del")
     * @Method("DELETE")
     */
    public function delAction(Request $request)
    {
        $user = $this->getUser();
        $vid = $request->get('vid');
        if(!$user || !$vid) {
            echo 'X-X';
            exit;
        }

        $em = $this->getDoctrine()->getManager();
        $video = $em->getRepository('MediaVideoBundle:Video')->find($vid);
        $vh = $em->getRepository('AppWebBundle:VideoHistory')->findOneBy(array('user'=>$user,'video'=>$video));
        if($vh) {
            $em->remove($vh);
            $em->flush();
            echo '^.^';
        } else {
            echo 'X-X';
        }
        exit;
    }
}
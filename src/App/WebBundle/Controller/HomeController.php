<?php

namespace App\WebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class HomeController extends Controller
{
    /**
     * @Route("/", name="app_web_home")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();
        //$cs = $em->getRepository('AppWebBundle:Course')->findAll();
        $random_videos = $em->getRepository('MediaVideoBundle:Video')->randomVideos();
        return array(
            'random_videos' => $random_videos,
          //'courses' => $cs,
        );
    }

}

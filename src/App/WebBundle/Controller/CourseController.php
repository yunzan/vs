<?php

namespace App\WebBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * Class CourseController
 * @Route("/course")
 */
class CourseController extends Controller
{
    const PER_PAGE = 12;
    /**
     * @Route("/", name="course")
     * @Template()
     */
    public function indexAction()
    {
        $query = array(); $courses = array();
        $scroll = intval($this->get('request')->get('scroll'));
        $order = $this->get('request')->get('order');
        $em = $this->getDoctrine()->getManager();
        $cr = $em->getRepository('AppWebBundle:Course');

        $courses = $cr->findBy(
            array(),
            array('position' => 'ASC'),
            self::PER_PAGE,
            $scroll * self::PER_PAGE
        );

        if($order) {
            $query = array(
              'order' => $order,
            );
        }

        if($scroll) {
            if(!$courses) {
                throw $this->createNotFoundException();
            }
            return $this->render('AppWebBundle:Course:scroll.html.twig', array(
              'courses' => $courses,
              'next_url' => $this->generateUrl('course', array_merge(array(
                'scroll' => $scroll+1,
              ),$query))
            ));
        }
        return array(
          'courses' => $courses,
          'next_url' => $this->generateUrl('course', array_merge(array('scroll' => 1,),$query)),
        );
    }

    /**
     * @Route("/{slug}", name="course_show")
     * @Template()
     */
    public function showAction($slug)
    {
        $em = $this->getDoctrine()->getManager();
        $cr = $em->getRepository('AppWebBundle:Course');
        $user = $this->getUser();
        $course = $cr->findOneBySlug($slug);
        if(!$course) {
            throw $this->createNotFoundException('Unable to find Course.');
        }
        $start_video = $cr->getStartVideo($course);
        $duration = $cr->getDuration($course);
        $chapters = $cr->getChapters($course);
        $cp = $em->getRepository('AppWebBundle:CourseProgress')->findOneBy(array('user'=> $user,'course'=>$course));
        $percent = $cp?$cp->getPercent():0;

        return array(
            'course' => $course,
            'start_video' => $start_video,
            'total' => array(
                'hours' => $cr->getHours($duration),
                'minutes' => $cr->getMinutes($duration),
            ),
            'chapters' => $chapters,
            'percent' => $percent,
        );
    }

    /**
     * @Route("/{slug}/video/{id}", name="course_video_play")
     * @Template()
     */
    public function playAction($slug, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $cr = $em->getRepository('AppWebBundle:Course');

        $course = $cr->findOneBySlug($slug);
        $videos = $cr->getVideos($course);
        foreach($videos as $key => $video) {
            if($video->getNumber() == $id) {
                $prev_video = isset($videos[$key-1])?$videos[$key-1]:null;
                $next_video = isset($videos[$key+1])?$videos[$key+1]:null;
                $current_video = $video;
            }
        }
        if(!$course || !isset($current_video)) {
            throw $this->createNotFoundException('Unable to find video in course.');
        }
        $chapters = $cr->getChapters($course);

        return array(
          'current_video' => $current_video,
          'chapters' => $chapters,
          'course' => $course,
          'prev_video' => $prev_video,
          'next_video' => $next_video,
        );

    }

}

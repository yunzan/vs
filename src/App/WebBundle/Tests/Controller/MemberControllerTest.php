<?php

namespace App\WebBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class MemberControllerTest extends WebTestCase
{
    public function testCourse()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/course');
    }

    public function testVideo()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/video');
    }

    public function testExperience()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/experience');
    }

    public function testSettings()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/settings');
    }

    public function testSettingsaccount()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/settings/account');
    }

    public function testSettingsprivacy()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/settings/privacy');
    }

    public function testSettingsnotifycation()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/settings/notifycation');
    }

    public function testSettingsmail()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/settings/mail');
    }

    public function testMessage()
    {
        $client = static::createClient();

        $crawler = $client->request('GET', '/message');
    }

}

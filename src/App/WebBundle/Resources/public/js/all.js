/*! js */
$(document).ready(function(e) {
	$('#searchp').find('input').focus(function(){
		$(this).parent().addClass('searchp2');
		$(this).parent().next().show();
	})
	$('#searchp').find('input').blur(function(){
		$(this).parent().removeClass('searchp2');
        var self = $(this);
        setTimeout(function(){
            self.parent().next().hide();
        }, 300)
	})


	var showhide = 1;
	$('.showhidebtn').click(function(){
		if(showhide == 1){
			$('.playmenu').animate({"right":"300px"},function(){
				$('.playobj').animate({"left":"150px"});
			});
			showhide = 0;
		}else{
			$('.playobj').animate({"left":"0px"},function(){
				$('.playmenu').animate({"right":"0px"});
			});
			showhide = 1;
		}
	})

	var righsh = 1;
	$('#hideRightBtn').click(function(){
		if(righsh == 1){
			$('#j-coursebox').css({"right":"0px"});
			$('#course-toolbar-box').hide();
			righsh = 0;
		}else{
			$('#j-coursebox').css({"right":"320px"});
			$('#course-toolbar-box').show();
			righsh = 1;
		}
	})

	$('#tabs').find('li').click(function(){
		var index = $(this).index();
		$('#tabs').find('li').removeClass('cur').eq(index).addClass('cur');
		$('#tabarea').find('.tabbox').hide().eq(index).show();
	})

	$('#cataloguelist').find('.first').find('p').each(function(index, element) {
		if($(this).hasClass('grayp')){
			$(this).next().show();
			$(this).removeClass('grayp');
			$(this).find('span').removeClass('hide').removeClass('show').addClass('hide');
		}else{
			$(this).next().hide();
			$(this).addClass('grayp');
			$(this).find('span').removeClass('hide').removeClass('show').addClass('show');
		}
	});
	$('#cataloguelist').find('.first').find('p').click(function(){
		if($(this).hasClass('grayp')){
			$(this).next().show();
			$(this).removeClass('grayp');
			$(this).find('span').removeClass('hide').removeClass('show').addClass('hide');
		}else{
			$(this).next().hide();
			$(this).addClass('grayp');
			$(this).find('span').removeClass('hide').removeClass('show').addClass('show');
		}
	})

});
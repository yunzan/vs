/**
 * Created by jeremy on 15-3-11.
 */
var StopPush = false;
var CountLimit = 100;
var Requested = 0;
var videoTracker = function(_url,_els){
    var url=_url,data = {};
    $.each(_els,function(idx, el){
        var item = {};
        if(typeof el == 'function') {
            item = el();
        } else if($(el).length) {
            item = {
                key: $(el).attr('key'),
                value: $(el).attr('value')
            }
        }
        eval('data.'+item.key +'='+item.value);
    });
    pushData(url,data);

    if(!StopPush) {
        setTimeout(function(){
            videoTracker(url,_els);
        }, 10000);
    }

}

var pushData = function(_url, _data) {
    if(!_data.played || !_data.duration){
        return;
    }
    $.ajax({
        url: _url,
        type: 'POST',
        data: _data,
        dateType: 'JSON',
        success: function(response){
            console.log(response);
            if(response == 'X-X') {
                StopPush = true;
            }
        },
        error: function(err){
            console.log(err);
            StopPush = true;
        }
    });

    checkStop(_data);
}

var checkStop = function(_data){
    Requested += 1;
    if(_data.duration && (_data.played == _data.duration)) {
        StopPush = true;
    }
    if(Requested > CountLimit) {
        StopPush = true;
    }
}
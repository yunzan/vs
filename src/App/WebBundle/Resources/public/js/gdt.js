// JavaScript Document

	
function CustomBar(o,top){
	   this.scrolls=o;
	   this.scrollarea=o.children[0];
	   this.scrollcontent=o.children[0].children[0];
	   this.scrollcolumn=o.children[1];
	   this.scrollbar=o.children[1].children[0];
	   this.mouseevent(top);
	   }	
	
CustomBar.prototype={
	mouseevent:function(top){
		var _this=this;
		var diffY;
		var events;
		this.scrollcolumn.style.height = this.scrollarea.offsetHeight + 'px';
		this.scrollarea.scrollTop = top;
		if(this.scrollarea.offsetHeight >= this.scrollcontent.offsetHeight){
				this.scrollbar.style.height = 0 + 'px';
				this.scrollcolumn.style.display = 'none';
			}else{
				this.scrollbar.style.height = (this.scrollarea.offsetHeight/this.scrollcontent.offsetHeight)
				*this.scrollcolumn.offsetHeight +'px';
			}
		var scale = (this.scrollarea.offsetHeight - this.scrollcontent.offsetHeight)
				  /(this.scrollbar.offsetHeight - this.scrollcolumn.offsetHeight);
						 
		function down(event){  
			events=event||window.event;
			events.preventDefault ? events.preventDefault() : events.returnValue=false;
			diffY=events.clientY - _this.scrollbar.offsetTop;			
			document.onmousemove=move;
			document.onmouseup=up;
		};
		
		function move(event){
			events=event||window.event;
			events.preventDefault ? events.preventDefault() : events.returnValue=false;	
			if(events.clientY - diffY<0){
				return false;
				}else if(events.clientY - diffY>_this.scrollbar.parentNode.offsetHeight-_this.scrollbar.offsetHeight){
						return false;
					}else{
							_this.scrollbar.style.top=(events.clientY - diffY) + "px";
							_this.scrollarea.scrollTop=(events.clientY - diffY)*scale;
						 }		
		};
			
		function up(){
			this.onmousemove=null;	
			this.onmousedown=null;	
		};
		
		function compatibleWheel(o){
			var delta=o;
			_this.scrollarea.scrollTop=_this.scrollarea.scrollTop - parseInt(delta)/6;
			_this.scrollbar.style.top=(_this.scrollarea.scrollTop - parseInt(delta)/6)/scale + "px";
								
			 if(_this.scrollarea.scrollTop==_this.scrollcontent.offsetHeight-_this.scrollarea.offsetHeight){
				_this.scrollarea.scrollTop=_this.scrollcontent.offsetHeight-_this.scrollarea.offsetHeight;									
			}
				
			if((_this.scrollarea.scrollTop - parseInt(delta)/6)/scale<0){
				_this.scrollbar.style.top= 0 +'px';	
			}else if((_this.scrollarea.scrollTop - parseInt(delta)/6)/scale
				          >_this.scrollcolumn.offsetHeight - _this.scrollbar.offsetHeight){
							
			_this.scrollbar.style.top =_this.scrollcolumn.offsetHeight - _this.scrollbar.offsetHeight + "px";
			
			}

	};		

		
	function mousewheels(){
		compatibleWheel(event.wheelDelta);
	};
	
	function firFoxwheels(event){
		compatibleWheel(event.detail*-40);
	}
	
	this.scrollbar.onmousedown=down;	
	if(document.all || window.navigator.userAgent.indexOf("Chrome") > -1){
		this.scrolls.onmousewheel=mousewheels;
	}else{
		this.scrolls.addEventListener('DOMMouseScroll',firFoxwheels,false);	}
	}
	

}


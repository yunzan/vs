<?php

namespace App\WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CourseVideo
 *
 * @ORM\Table(name="app__course_video")
 * @ORM\Entity
 */
class CourseVideo
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\JoinColumn(name="course_id", referencedColumnName="id")
     * @ORM\ManyToOne(targetEntity="App\WebBundle\Entity\Course")
     */
    private $course;

    /**
     * @ORM\JoinColumn(name="video_id", referencedColumnName="id")
     * @ORM\OneToOne(targetEntity="Media\VideoBundle\Entity\Video")
     */
    private $video;

    /**
     * @var string
     *
     * @ORM\Column(name="chapter", type="string", length=255)
     */
    private $chapter;

    /**
     * @var integer
     *
     * @ORM\Column(name="delta", type="integer")
     */
    private $delta = 0;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set chapter
     *
     * @param string $chapter
     * @return CourseVideo
     */
    public function setChapter($chapter)
    {
        $this->chapter = $chapter;

        return $this;
    }

    /**
     * Get chapter
     *
     * @return string 
     */
    public function getChapter()
    {
        return $this->chapter;
    }

    /**
     * Set delta
     *
     * @param integer $delta
     * @return CourseVideo
     */
    public function setDelta($delta)
    {
        $this->delta = $delta;

        return $this;
    }

    /**
     * Get delta
     *
     * @return integer 
     */
    public function getDelta()
    {
        return $this->delta;
    }

    /**
     * Set course
     *
     * @param \App\WebBundle\Entity\Course $course
     * @return CourseVideo
     */
    public function setCourse(\App\WebBundle\Entity\Course $course)
    {
        $this->course = $course;

        return $this;
    }

    /**
     * Get course
     *
     * @return \App\WebBundle\Entity\Course 
     */
    public function getCourse()
    {
        return $this->course;
    }

    /**
     * Set video
     *
     * @param \Media\VideoBundle\Entity\Video $video
     * @return CourseVideo
     */
    public function setVideo(\Media\VideoBundle\Entity\Video $video)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get video
     *
     * @return \Media\VideoBundle\Entity\Video 
     */
    public function getVideo()
    {
        return $this->video;
    }
}

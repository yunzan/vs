<?php

namespace App\WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * CourseStatics
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="App\WebBundle\Entity\CourseStaticsRepository")
 */
class CourseStatics
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Course
     * @ORM\JoinColumn(name="course_id", referencedColumnName="id")
     * @ORM\OneToOne(targetEntity="App\WebBundle\Entity\Course")
     */
    private $course;
    /**
     * @var integer
     *
     * @ORM\Column(name="play", type="integer")
     */
    private $play = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="collect", type="integer")
     */
    private $collect = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="comment", type="integer")
     */
    private $comment = 0;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set play
     *
     * @param integer $play
     * @return CourseStatics
     */
    public function setPlay($play)
    {
        $this->play = $play;

        return $this;
    }

    /**
     * Get play
     *
     * @return integer 
     */
    public function getPlay()
    {
        return $this->play;
    }

    /**
     * Set collect
     *
     * @param integer $collect
     * @return CourseStatics
     */
    public function setCollect($collect)
    {
        $this->collect = $collect;

        return $this;
    }

    /**
     * Get collect
     *
     * @return integer 
     */
    public function getCollect()
    {
        return $this->collect;
    }

    /**
     * Set comment
     *
     * @param integer $comment
     * @return CourseStatics
     */
    /**
     * Collect once
     *
     * @Return VideoStatics
     */
    public function collectOnce()
    {
        $this->collect += 1;

        return $this;
    }

    /**
     * UnCollect once
     *
     * @Return VideoStatics
     */
    public function unCollectOnce()
    {
        $this->collect -= 1;

        return $this;
    }
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return integer 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set course
     *
     * @param \App\WebBundle\Entity\Course $course
     * @return CourseStatics
     */
    public function setCourse(\App\WebBundle\Entity\Course $course = null)
    {
        $this->course = $course;

        return $this;
    }

    /**
     * Get course
     *
     * @return \App\WebBundle\Entity\Course 
     */
    public function getCourse()
    {
        return $this->course;
    }
}

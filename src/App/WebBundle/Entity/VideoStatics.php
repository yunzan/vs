<?php

namespace App\WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VideoStatics
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="App\WebBundle\Entity\VideoStaticsRepository")
 */
class VideoStatics
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Video
     * @ORM\JoinColumn(name="video_id", referencedColumnName="id")
     * @ORM\OneToOne(targetEntity="Media\VideoBundle\Entity\Video")
     */
    private $video;

    /**
     * @var integer
     *
     * @ORM\Column(name="play", type="integer")
     */
    private $play = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="collect", type="integer")
     */
    private $collect = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="comment", type="integer")
     */
    private $comment = 0;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set play
     *
     * @param integer $play
     * @return VideoStatics
     */
    public function setPlay($play)
    {
        $this->play = $play;

        return $this;
    }

    /**
     * Get play
     *
     * @return integer 
     */
    public function getPlay()
    {
        return $this->play;
    }

    /**
     * Play once
     *
     * @return VideoStatics
     */
    public function playOnce()
    {
        $this->play += 1;

        return $this;
    }

    /**
     * Set collect
     *
     * @param integer $collect
     * @return VideoStatics
     */
    public function setCollect($collect)
    {
        $this->collect = $collect;

        return $this;
    }

    /**
     * Get collect
     *
     * @return integer 
     */
    public function getCollect()
    {
        return $this->collect;
    }

    /**
     * Collect once
     *
     * @Return VideoStatics
     */
    public function collectOnce()
    {
        $this->collect += 1;

        return $this;
    }

    /**
     * UnCollect once
     *
     * @Return VideoStatics
     */
    public function unCollectOnce()
    {
        $this->collect -= 1;

        return $this;
    }

    /**
     * Set comment
     *
     * @param integer $comment
     * @return VideoStatics
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return integer 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Comment once
     *
     * @Return VideoStatics
     */
    public function commentOnce()
    {
        $this->comment += 1;

        return $this;
    }

    /**
     * Set video
     *
     * @param \Media\VideoBundle\Entity\Video $video
     * @return VideoStatics
     */
    public function setVideo(\Media\VideoBundle\Entity\Video $video = null)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get video
     *
     * @return \Media\VideoBundle\Entity\Video 
     */
    public function getVideo()
    {
        return $this->video;
    }
}

<?php

namespace App\WebBundle\Entity;

use Doctrine\ORM\EntityRepository;

class CourseRepository extends EntityRepository
{
    const RESULT_MIN = 10;
    public function findVC(\App\WebBundle\Entity\Course $course = null)
    {
        $query = $this->getEntityManager()
          ->createQuery(
            'SELECT v,cv,c FROM AppWebBundle:CourseVideo cv
              JOIN cv.video v
              JOIN cv.course c
              WHERE c.id = :cid
              ORDER BY cv.delta ASC'
          )
          ->setParameter('cid', $course->getId());
        try {
            return $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }

    }

    public function searchResult($word = '')
    {
        $query = $this->getEntityManager()
          ->createQuery(
            'SELECT c FROM AppWebBundle:Course c
              WHERE c.title LIKE :word'
          )
          ->setMaxResults(self::RESULT_MIN)
          ->setParameter('word', "%$word%");
        try {
            $res = $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            $res = null;
        }

        $less = self::RESULT_MIN - count($res);

        if($less) {
            $query = $this->getEntityManager()
              ->createQuery(
                'SELECT c FROM AppWebBundle:Course c
              WHERE c.description LIKE :word'
              )
              ->setMaxResults($less)
              ->setParameter('word', "%$word%");
            try {
                $res += $query->getResult();
            } catch (\Doctrine\ORM\NoResultException $e) {
                return $res;
            }
        }

        return $res;

    }

    public function getDuration(\App\WebBundle\Entity\Course $course = null)
    {
        $duration = 0;
        $res = $this->findVC($course);

        foreach($res as $r) {
            $duration += $r->getVideo()->getDuration();
        }

        return $duration;
    }

    public function getHours($duration = 0)
    {
        $hours = floor($duration/3600);
        return $hours = $hours > 0 ? $hours : 0;
    }
    public function getMinutes($duration = 0)
    {
        $minutes = floor($duration%3600/60);
        return $minutes = $minutes > 0 ? $minutes : 0;
    }

    public function getChapters(\App\WebBundle\Entity\Course $course = null)
    {
        $chapters = array();
        $res = $this->findVC($course);


        foreach($res as $r) {
            $chapters[$r->getChapter()]['title'] = $r->getChapter();
            $chapters[$r->getChapter()]['videos'][] = $r->getVideo();
        }

        return $chapters;
    }

    public function getStartVideo(\App\WebBundle\Entity\Course $course = null)
    {
        $res = $this->findVC($course);
        if(empty($res)) {
            return null;
        }
        return $res[0]->getVideo();
    }

    public function getVideos(\App\WebBundle\Entity\Course $course = null)
    {
        $videos = array();
        $res = $this->findVC($course);
        if(empty($res)) {
            return null;
        }
        foreach($res as $r) {
            $videos[] = $r->getVideo();
        }

        return $videos;
    }

}
<?php

namespace App\WebBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * VideoHistory
 *
 * @ORM\Table()
 * @ORM\Entity(repositoryClass="App\WebBundle\Entity\VideoHistoryRepository")
 */
class VideoHistory
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var User
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @ORM\ManyToOne(targetEntity="App\UserBundle\Entity\User")
     */
    private $user;

    /**
     * @var Video
     * @ORM\JoinColumn(name="video_id", referencedColumnName="id")
     * @ORM\ManyToOne(targetEntity="Media\VideoBundle\Entity\Video")
     */
    private $video;

    /**
     * @var Course
     * @ORM\JoinColumn(name="course_id", referencedColumnName="id", nullable=true)
     * @ORM\ManyToOne(targetEntity="App\WebBundle\Entity\Course")
     */
    private $course;

    /**
     * @var integer
     *
     * @ORM\Column(name="played", type="integer")
     */
    private $played;

    /**
     * @var integer
     *
     * @ORM\Column(name="duration", type="integer")
     */
    private $duration;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated", type="datetime")
     */
    private $updated;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetime")
     */
    private $created;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set played
     *
     * @param integer $played
     * @return VideoHistory
     */
    public function setPlayed($played)
    {
        $this->played = $played;

        return $this;
    }

    /**
     * Get played
     *
     * @return integer 
     */
    public function getPlayed()
    {
        return $this->played;
    }

    /**
     * Set duration
     *
     * @param integer $duration
     * @return VideoHistory
     */
    public function setDuration($duration)
    {
        $this->duration = $duration;

        return $this;
    }

    /**
     * Get duration
     *
     * @return integer 
     */
    public function getDuration()
    {
        return $this->duration;
    }

    /**
     * Set updated
     *
     * @param \DateTime $updated
     * @return VideoHistory
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return \DateTime 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return VideoHistory
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set user
     *
     * @param \App\UserBundle\Entity\User $user
     * @return VideoHistory
     */
    public function setUser(\App\UserBundle\Entity\User $user = null)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return \App\UserBundle\Entity\User 
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set video
     *
     * @param \Media\VideoBundle\Entity\Video $video
     * @return VideoHistory
     */
    public function setVideo(\Media\VideoBundle\Entity\Video $video = null)
    {
        $this->video = $video;

        return $this;
    }

    /**
     * Get video
     *
     * @return \Media\VideoBundle\Entity\Video 
     */
    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Set course
     *
     * @param \App\WebBundle\Entity\Course $course
     * @return VideoHistory
     */
    public function setCourse(\App\WebBundle\Entity\Course $course = null)
    {
        $this->course = $course;

        return $this;
    }

    /**
     * Get course
     *
     * @return \App\WebBundle\Entity\Course
     */
    public function getCourse()
    {
        return $this->course;
    }
}

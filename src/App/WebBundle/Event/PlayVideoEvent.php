<?php
/**
 * Created by PhpStorm.
 * User: Jeremy Hu
 * Date: 4/5/15
 * Time: 10:32 PM
 */

namespace App\WebBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use Media\VideoBundle\Entity\Video;
use App\UserBundle\Entity\User;

class PlayVideoEvent extends Event
{
    protected $video;

    protected $user;

    public function __construct(Video $video, User $user)
    {
        $this->video = $video;
        $this->user  = $user;
    }

    public function getVideo()
    {
        return $this->video;
    }

    public function getUser()
    {
        return $this->user;
    }

}
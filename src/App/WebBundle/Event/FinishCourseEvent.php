<?php
/**
 * Created by PhpStorm.
 * User: Jeremy Hu
 * Date: 4/6/15
 * Time: 5:48 PM
 */

namespace App\WebBundle\Event;

use Symfony\Component\EventDispatcher\Event;
use App\WebBundle\Entity\Course;
use App\UserBundle\Entity\User;

class FinishCourseEvent extends Event
{
    protected $course;

    protected $user;

    public function __construct(Course $course, User $user)
    {
        $this->course = $course;
        $this->user   = $user;
    }

    public function getCourse()
    {
        return $this->course;
    }

    public function getUser()
    {
        return $this->user;
    }

}
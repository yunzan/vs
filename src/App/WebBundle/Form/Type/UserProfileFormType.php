<?php

namespace App\WebBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use App\UserBundle\Models\User;

class UserProfileFormType extends AbstractType
{
  private $class;

  function __construct($class)
  {
    $this->class = $class;
  }

  public function buildForm(FormBuilderInterface $builder, array $options)
  {
      $builder
          ->add('nickname', null, array(
              'attr' => array(
                  'class' => 'icons intputtext',
              ),
          ))
          ->add('email', 'email', array(
              'attr' => array(
                  'class' => 'icons intputtext',
              ),
          ))
          ->add('signature', 'textarea', array(
              'attr' => array(
                  'class' => 'textarea g-u',
              ),
              'required' => false,
          ))
          ->add('avatar', 'hidden', array('required' => false,))
          ->add('gender', 'choice', array(
              'choices' => array(
                  User::GENDER_MALE => '男',
                  User::GENDER_FEMALE => '女',
                  User::GENDER_SECRET => '保密',
              ),
              'multiple' => false,
              'required' => true,
              'expanded' => true,
          ))
      ;
  }

  public function setDefaultOptions(OptionsResolverInterface $resolver)
  {
    $resolver->setDefaults(array(
        'data_class' => $this->class,
    ));
  }

  /**
   * Returns the name of this type.
   *
   * @return string The name of this type
   */
  public function getName() {
    return 'user_profile';
  }


}
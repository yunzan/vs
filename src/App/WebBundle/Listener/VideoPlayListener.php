<?php
/**
 * Created by PhpStorm.
 * User: Jeremy Hu
 * Date: 4/5/15
 * Time: 11:27 PM
 */

namespace App\WebBundle\Listener;

use App\WebBundle\Event\FinishCourseEvent;
use App\WebBundle\VideoEvents;
use App\WebBundle\Event\PlayVideoEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use App\UserBundle\Entity\Experience;
use App\UserBundle\Models\ExpOptions;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;



class VideoPlayListener implements EventSubscriberInterface
{
    private $doctrine;

    private $router;
    function __construct($doctrine, UrlGeneratorInterface $router)
    {
        $this->doctrine = $doctrine;
        $this->router = $router;
    }

    /**
     * Returns an array of event names this subscriber wants to listen to.
     *
     * The array keys are event names and the value can be:
     *
     *  * The method name to call (priority defaults to 0)
     *  * An array composed of the method name to call and the priority
     *  * An array of arrays composed of the method names to call and respective
     *    priorities, or 0 if unset
     *
     * For instance:
     *
     *  * array('eventName' => 'methodName')
     *  * array('eventName' => array('methodName', $priority))
     *  * array('eventName' => array(array('methodName1', $priority), array('methodName2'))
     *
     * @return array The event names to listen to
     *
     * @api
     */
    public static function getSubscribedEvents()
    {
        return array(
            VideoEvents::VIDEO_PLAY => 'onVideoPlay',
            VideoEvents::VIDEO_COURSE_FINISH => 'onVideoCourseFinish',
        );
    }

    public function onVideoPlay(PlayVideoEvent $event)
    {
        $em = $this->doctrine->getManager();
        $user = $event->getUser();
        $video = $event->getVideo();
        $type = ExpOptions::PLAY_A_VIDEO;
        $uri = $this->router->generate('video_show',array('slug'=>$video->getNumber()));
        $exp = $em->getRepository('App\UserBundle\Entity\Experience')->findOneBy(array(
            'uri' => $uri,
            'user'  => $user,
            'type'  => $type,
        ));
        if($exp){
            return;
        }
        $exp = new Experience();

        $points = ExpOptions::$points[$type];

        $exp->setCreated(new \DateTime());
        $exp->setPoints($points);
        $exp->setTitle($video->getTitle());
        $exp->setType($type);
        $exp->setUri($uri);
        $exp->setUser($user);

        $user->setExperience($user->getExperience()+$points);

        $em->persist($exp);
        $em->persist($user);

        $em->flush();
    }

    public function onVideoCourseFinish(FinishCourseEvent $event)
    {
        $em = $this->doctrine->getManager();
        $user = $event->getUser();
        $course = $event->getCourse();
        $uri = $this->router->generate('course_show',array('slug'=>$course->getSlug()));
        $type = ExpOptions::FINISHED_A_COURSE;
        $exp = $em->getRepository('App\UserBundle\Entity\Experience')->findOneBy(array(
            'uri' => $uri,
            'user'  => $user,
            'type'  => $type,
        ));
        if($exp){
            return;
        }
        $exp = new Experience();

        $points = ExpOptions::$points[$type];

        $exp->setCreated(new \DateTime());
        $exp->setPoints($points);
        $exp->setTitle($course->getTitle());
        $exp->setType($type);
        $exp->setUri($uri);
        $exp->setUser($user);

        $user->setExperience($user->getExperience()+$points);

        $em->persist($exp);
        $em->persist($user);

        $em->flush();
    }
}
<?php
/**
 * Copyright (C) Alibaba Cloud Computing
 * All rights reserved.
 *
 * 版权所有 （C）阿里云计算有限公司
 */
namespace App\CommonBundle\Utilities;

class ServiceConstants {
    const CHARSET = 'utf-8';
    const SDK_VERSION = '2.0.0-beta';
}

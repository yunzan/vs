<?php
/**
 * Copyright (C) Alibaba Cloud Computing
 * All rights reserved.
 *
 * 版权所有 （C）阿里云计算有限公司
 */
namespace App\CommonBundle\Utilities;

class HttpMethods {
    const GET = 'GET';
    const PUT = 'PUT';
    const POST = 'POST';
    const HEAD = 'HEAD';
    const DELETE = 'DELETE';
}

<?php
/**
 * Created by PhpStorm.
 * User: Jeremy Hu
 * Date: 4/6/15
 * Time: 12:34 AM
 */

namespace App\UserBundle\Models;


class ExpOptions
{
    /**
     * Types
     */
    const PLAY_A_VIDEO = 'play_a_video';
    const FINISHED_A_COURSE = 'finished_a_course';

    static $types = array(
        self::PLAY_A_VIDEO => '播放了一个视频',
        self::FINISHED_A_COURSE => '完成了一个课程',
    );

    /**
     * Exp points
     */
    static $points = array(
        self::PLAY_A_VIDEO => 10,
        self::FINISHED_A_COURSE => 50,
    );
}
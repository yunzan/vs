<?php

namespace App\UserBundle\Models;


class User {
    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;
    const GENDER_SECRET = 0;
}
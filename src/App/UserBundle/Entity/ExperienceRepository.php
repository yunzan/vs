<?php

namespace App\UserBundle\Entity;

use Doctrine\ORM\EntityRepository;

/**
 * ExperienceRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ExperienceRepository extends EntityRepository
{
    public function getTotalPointsByUser(\App\UserBundle\Entity\User $user)
    {
        $points = 0;
        $entities = $this->findByUser($user);
        foreach($entities as $exp) {
            $points += $exp->getPoints();
        }
        return $points;
    }

    public function getLoserPercent(\App\UserBundle\Entity\User $user)
    {
        $total = $this->getEntityManager()
          ->createQuery(
            'SELECT COUNT(u) from AppUserBundle:User u'
          )
          ->getSingleScalarResult();
        $loser = $this->getEntityManager()
          ->createQuery(
            'SELECT COUNT(u) from AppUserBundle:User u
            WHERE u.experience < :exp'
          )
          ->setParameter('exp', $this->getTotalPointsByUser($user))
          ->getSingleScalarResult();

        if($loser && $total) {
            return number_format($loser / $total, 2) * 100;
        }else {
            return 0;
        }

    }

    public function listResult($limit = 10, $offset = 0)
    {
        try{
            $result = $this->getEntityManager()
                ->createQuery(
                    'SELECT exp FROM AppUserBundle:Experience exp
                WHERE 1 = 1 ORDER BY exp.created DESC'
                )
                ->setFirstResult($offset)
                ->setMaxResults($limit)
                ->getResult();
        } catch(\Doctrine\ORM\NoResultException $e){
            return null;
        }

        return $result;
    }

}

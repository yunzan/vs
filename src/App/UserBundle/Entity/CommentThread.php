<?php

namespace App\UserBundle\Entity;


use Doctrine\ORM\Mapping as ORM;
use FOS\CommentBundle\Entity\Thread as BaseThread;

/**
 * @ORM\Entity
 * @ORM\Table("app__comment_thread")
 * @ORM\ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 */
class CommentThread extends BaseThread
{
    /**
     * @var string $id
     *
     * @ORM\Id
     * @ORM\Column(type="string")
     */
    protected $id;
}
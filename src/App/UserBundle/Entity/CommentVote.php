<?php

namespace App\UserBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use FOS\CommentBundle\Entity\Vote as BaseVote;

/**
 * @ORM\Entity
 * @ORM\Table(name="app__comment_vote")
 * @ORM\ChangeTrackingPolicy("DEFERRED_EXPLICIT")
 */
class CommentVote extends BaseVote
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Comment of this vote
     *
     * @var Comment
     * @ORM\ManyToOne(targetEntity="App\UserBundle\Entity\Comment")
     */
    protected $comment;
}
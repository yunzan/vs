<?php

namespace App\AdminBundle\Models;


class OSSVideoOptions extends UploadOptions {
    const ACCESS_KEY_ID = 'video_oss_key';
    const ACCESS_KEY_SECRET = 'video_oss_secret';
    const ENDPOINT = 'video_oss_endpoint';
    const BUCKET = 'video_oss_bucket';

    const UPLOAD_TMP_DIR = '/tmp/video';

    const HOST = 'http://v.veishu.com/';
}
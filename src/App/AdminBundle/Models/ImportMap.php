<?php
/**
 * Created by PhpStorm.
 * User: jeremy
 * Date: 14-12-17
 * Time: 下午11:57
 */

namespace App\AdminBundle\Models;


class ImportMap {
    const NUMBER = 'number';
    const TITLE = 'title';
    const FORMAT = 'format';
    const DURATION = 'duration';
    const COVER = 'cover';
    const SOURCE = 'source';
    const DESCRIPTION = 'description';
    const SPEAKER = 'speaker';
    const SPEAKER_TITLE = 'speakerTitle';
    const SPEAKER_AVATAR = 'speakerAvatar';
    const SPEAKER_INTRO = 'speakerIntro';
    const CREATED = 'created';
    const SIZE = 'size';
    const ALLOW_SALE = 'allowSale';
    const SLUG = 'slug';
    const NAME = 'name';
    const POSITION = 'position';
    const COURSE_NUMBER = 'courseNumber';
    const COURSE_TITLE = 'courseTitle';
    const COURSE_COVER = 'courseCover';
    const COURSE_DESC = 'courseDesc';
    const COURSE_CHAPTER = 'courseChapter';
    const COURSE_SLUG = 'courseSlug';
    const DELTA_IN_COURSE = 'deltaInCourse';
    const TERM = 'term';
    const POINT = 'point';
    const SUMMARY = 'summary';
    const TERM_SLUG = 'termSlug';
    const CASE_SLUG = 'caseSlug';
}
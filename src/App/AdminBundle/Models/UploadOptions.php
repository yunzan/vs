<?php

namespace App\AdminBundle\Models;


class UploadOptions {
    const UPLOAD_TMP_DIR = '/tmp/upload';
    const UPLOAD_PARAM = 'file';
    const UPLOAD_METHOD_POST = 'POST';
    const SLASH = '/';
    const CACHE_CONTROL = 'public,max-age=604800'; //7天
    const EXPIRE = '+7 days';
    const CONTENT_ENCODING = 'utf-8';
}
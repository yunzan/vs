<?php

namespace App\AdminBundle\Models;


class OSSImageOptions extends UploadOptions {
    const ACCESS_KEY_ID = 'image_oss_key';
    const ACCESS_KEY_SECRET = 'image_oss_secret';
    const ENDPOINT = 'image_oss_endpoint';
    const BUCKET = 'image_oss_bucket';
    const CONTENT_DISPOSITION = 'inline;';
    const UPLOAD_TMP_DIR = '/tmp/image';

    const HOST = 'http://img.veishu.com/';
}
<?php

namespace App\AdminBundle\Form;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class Import extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('file', 'file', array(
          'label' => $options['label']['file'],
          'attr' => array(
            'class' => 'btn',
          )
        ));
    }

    public function getName()
    {
        return 'import';
    }
}
<?php

namespace App\AdminBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;
use Media\VideoBundle\Entity\Video;
use Media\VideoBundle\Models\Video as V;

class VideoType extends AbstractType
{

    private $class;

    function __construct($class)
    {
        $this->class = $class;
    }
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('number')
            ->add('title')
            ->add('format', null, array('required' => false))
            ->add('duration')
            ->add('cover')
            ->add('source')
            ->add('description', null, array('required' => false))
            ->add('speaker', null, array('required' => true))
            ->add('speakerTitle', null, array('required' => true))
            ->add('speakerAvatar', null, array('required' => false))
            ->add('speakerIntro', 'textarea', array('required' => false))
            ->add('size', null, array('required' => false))
            ->add('allowSale', null, array('required' => false))
            ->add('onlyInCourse', null, array('required' => false))
            ->add('status', 'choice', array('choices'=>array(
                V::STATUS_PUBLISHED => '发布',
                V::STATUS_UNPUBLISHED => '下架',
            )))
            ->add('slug', null, array('required' => false))
            ->add('position', null, array('required' => false))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => $this->class,
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'media_video';
    }
}

<?php

namespace App\AdminBundle\Form;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;

class Login extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('username', 'text', array(
          'label' => $options['label']['username'],
          'attr' => array(
            'class' => 'form-control',
          )
        ));
        $builder->add('password', 'password', array(
          'label' => $options['label']['password'],
          'attr' => array(
            'class' => 'form-control',
          )
        ));
    }

    public function getName()
    {
        return 'login';
    }
}
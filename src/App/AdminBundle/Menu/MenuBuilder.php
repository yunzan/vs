<?php

namespace App\AdminBundle\Menu;

use Knp\Menu\FactoryInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContextInterface;

class MenuBuilder {
    private $factory;

    private $securityContext;

    /**
     * @param FactoryInterface $factory
     */
    public function __construct(FactoryInterface $factory, SecurityContextInterface $securityContext)
    {
        $this->factory = $factory;
        $this->securityContext = $securityContext;
    }

    public function createAdminMenu(Request $request)
    {
        $menu = $this->factory->createItem('root',array('childrenAttributes'=>array('class'=>'nav nav-sidebar')));

        $menu->addChild('总览面板', array('route' => 'app_admin_dashboard_index',));
        $menu->addChild('视频信息', array('route' => 'admin_video',));
        $menu->addChild('视频文件', array('route' => 'app_admin_video_index',));
        $menu->addChild('图片文件', array('route' => 'app_admin_image_index',));
        $menu->addChild('分类管理', array('route' => 'admin_category',));
        $menu->addChild('课程管理', array('route' => 'admin_course',));
        $menu->addChild('数据导入', array('route' => 'app_admin_import_video',));

        switch($request->get('_route')){
            case 'admin_video':
            case 'admin_video_new':
            case 'admin_video_edit':
                $menu['视频信息']->setCurrent(true);
                break;
            case 'app_admin_video_upload':
                $menu['视频文件']->setCurrent(true);
                break;
            case 'app_admin_image_upload':
                $menu['图片文件']->setCurrent(true);
                break;
            default:
                break;
        }

        return $menu;

    }

}
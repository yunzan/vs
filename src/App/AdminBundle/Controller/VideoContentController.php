<?php

namespace App\AdminBundle\Controller;

use App\WebBundle\Entity\CourseVideo;
use Media\VideoBundle\Entity\Snapshot;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Media\VideoBundle\Entity\Video;
use Media\VideoBundle\Entity\VideoTerm;
use App\CategoryBundle\Entity\Category;
use App\CategoryBundle\Entity\Term;
use App\AdminBundle\Form\Type\VideoType as VideoType;

/**
 * Video controller.
 *
 * @Route("/video/content")
 */
class VideoContentController extends Controller
{

    /**
     * Lists all Video entities.
     *
     * @Route("/", name="admin_video")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('MediaVideoBundle:Video')->findAll();

        return array(
            'entities' => $entities,
        );
    }
    /**
     * Creates a new Video entity.
     *
     * @Route("/", name="admin_video_create")
     * @Method("POST")
     * @Template("MediaVideoBundle:Video:new.html.twig")
     */
    public function createAction(Request $request)
    {
        $entity = new Video();
        $form = $this->createCreateForm($entity);
        $form->handleRequest($request);

        $em = $this->getDoctrine()->getManager();
        $entity->setCreatedValue();
        $em->persist($entity);

        //检查编号重复
        $exist = $em->getRepository('MediaVideoBundle:Video')->findOneByNumber($entity->getNumber());
        if($exist) {
            $form->addError(new FormError('编号重复'));
        }

        //处理分类
        $term = $em->getRepository('AppCategoryBundle:Term')->find($request->get('term')['id']);
        if(!$term) {
            $form->addError(new FormError('term'));
        }
        else if($term->getCategory()->getId() != $request->get('category')['id']) {
            $form->addError(new FormError('category'));
        } else {
            //预存分类信息
            $videoTerm = new VideoTerm();
            $videoTerm->setVideo($entity);
            $videoTerm->setTerm($term);
            $em->persist($videoTerm);
        }
        //处理课程
        if(!empty($request->get('course')['id']) && !empty($request->get('course_video')['chapter'])){
            $course = $em->getRepository('AppWebBundle:Course')->find($request->get('course')['id']);
            if($course) {
                //新增
                $courseVideo = new CourseVideo();
                $courseVideo->setCourse($course);
                $courseVideo->setVideo($entity);
                $courseVideo->setChapter($request->get('course_video')['chapter']);
                $courseVideo->setDelta($request->get('course_video')['delta']);
                $em->persist($courseVideo);
            }
        }
        //处理知识点
        foreach($request->get('snapshot')['point'] as $k => $p) {
            if($p > 0 && !empty($request->get('snapshot')['summary'][$k])) {
                $snapshot = new Snapshot();
                $snapshot->setVideo($entity);
                $snapshot->setPoint(intval($p));
                $snapshot->setSummary($request->get('snapshot')['summary'][$k]);
                $em->persist($snapshot);
            }
        }

        if ($form->isValid()) {
            $em->flush();

            $request->getSession()->getFlashBag()->add(
              'success',
              '创建成功!'
            );
        } else {
            $errors = $form->getErrors();
            $current = $errors->current();
            $message = '<strong>创建失败!</strong>';
            if($current instanceof FormError){
                $message .= '<br /> 错误信息：' . $current->getMessage();
            }

            $request->getSession()->getFlashBag()->add(
              'failed',
              $message
            );

        }

        return $this->redirect($this->generateUrl('admin_video_new'));
    }

    /**
     * Creates a form to create a Video entity.
     *
     * @param Video $entity The entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createCreateForm(Video $entity)
    {
        $form = $this->createForm('media_video', $entity, array(
            'action' => $this->generateUrl('admin_video_create'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit', array('label' => 'Create'));

        return $form;
    }

    /**
     * Displays a form to create a new Video entity.
     *
     * @Route("/new", name="admin_video_new")
     * @Method("GET")
     * @Template()
     */
    public function newAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entity = new Video();
        $form   = $this->createCreateForm($entity);

        $categories = $em->getRepository('AppCategoryBundle:Category')->findAll();
        $terms      = $em->getRepository('AppCategoryBundle:Term')->findAll();
        $term       = $em->getRepository('AppCategoryBundle:Term')->findOneBy(array('category' => $categories[0]));
        $cate_terms = $em->getRepository('AppCategoryBundle:Term')->findByCategory($term->getCategory());
        $courses    = $em->getRepository('AppWebBundle:Course')->findAll();

        $terms_json = [];
        foreach($terms as $t) {
            $terms_json[$t->getCategory()->getId()][$t->getId()] = $t->getName();
        }
        $terms_json = json_encode($terms_json);

        return array(
          'entity'     => $entity,
          'form'       => $form->createView(),
          'categories' => $categories,
          'cate_terms' => $cate_terms,
          'cid'        => $term->getCategory()->getId(), //默认一级分类ID
          'tid'        => $term->getId(),                //默认二级分类ID
          'courses'    => $courses,
          'terms_json' => $terms_json,
        );
    }

    /**
     * Finds and displays a Video entity.
     *
     * @Route("/{id}", name="admin_video_show")
     * @Method("GET")
     * @Template()
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MediaVideoBundle:Video')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Video entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        );
    }

    /**
     * Displays a form to edit an existing Video entity.
     *
     * @Route("/{id}/edit", name="admin_video_edit")
     * @Method("GET")
     * @Template()
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MediaVideoBundle:Video')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Video entity.');
        }

        $categories = $em->getRepository('AppCategoryBundle:Category')->findAll();
        $terms = $em->getRepository('AppCategoryBundle:Term')->findAll();
        $termVideo = $em-> getRepository('MediaVideoBundle:VideoTerm')->findOneByVideo($entity);
        $term = $termVideo?$termVideo->getTerm():null;
        $defaultCategory = $term?$term->getCategory():$em->getRepository('AppCategoryBundle:Category')->findOne();
        $cate_terms = $em->getRepository('AppCategoryBundle:Term')->findByCategory($defaultCategory);
        $courses = $em->getRepository('AppWebBundle:Course')->findAll();
        $courseVideo = $em->getRepository('AppWebBundle:CourseVideo')->findOneByVideo($entity);
        $course = $courseVideo?$courseVideo->getCourse():null;
        $snapshots = $em->getRepository('MediaVideoBundle:Snapshot')->findByVideo($entity, array('point' => 'ASC'));

        $editForm = $this->createEditForm($entity);
        $deleteForm = $this->createDeleteForm($id);

        $terms_json = array();
        foreach($terms as $t) {
            $terms_json[$t->getCategory()->getId()][$t->getId()] = $t->getName();
        }
        $terms_json = json_encode($terms_json);
        return array(
          'entity'      => $entity,
          'edit_form'   => $editForm->createView(),
          'delete_form' => $deleteForm->createView(),
          'categories'  => $categories,                   //所有分类，用于遍历一级分类列表
          'cate_terms'  => $cate_terms,                   //当前一级分类的所有二级分类
          'terms_json'  => $terms_json,                   //所有二级分类，制作二级联动
          'cid'         => $term->getCategory()->getId(), //当前一级分类ID
          'tid'         => $term->getId(),                //当前二级分类ID
          'courses'     => $courses,
          'csid'        => $course?$course->getId():0,
          'delta'       => $courseVideo?$courseVideo->getDelta():0,
          'chapter'     => $courseVideo?$courseVideo->getChapter():'',
          'snapshots'   => $snapshots?$snapshots:null,
        );
    }

    /**
    * Creates a form to edit a Video entity.
    *
    * @param Video $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Video $entity)
    {
        $form = $this->createForm('media_video', $entity, array(
            'action' => $this->generateUrl('admin_video_update', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }
    /**
     * Edits an existing Video entity.
     *
     * @Route("/{id}", name="admin_video_update")
     * @Method("PUT")
     * @Template("MediaVideoBundle:Video:edit.html.twig")
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('MediaVideoBundle:Video')->find($id);
        $original_number = $entity->getNumber();

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Video entity.');
        }
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if($entity->getNumber() != $original_number) {
            //检查编号重复
            $exist = $em->getRepository('MediaVideoBundle:Video')->findOneByNumber($entity->getNumber());
            if($exist) {
                $editForm->addError(new FormError('编号重复'));
            }
        }

        //处理分类
        $term = $em->getRepository('AppCategoryBundle:Term')->find($request->get('term')['id']);
        if(!$term) {
            $editForm->addError(new FormError('term'));
        }
        else if($term->getCategory()->getId() != $request->get('category')['id']) {
            $editForm->addError(new FormError('category'));
        } else {
            //预存分类信息
            $videoTerm = $em->getRepository('MediaVideoBundle:VideoTerm')->findOneByVideo($entity);
            if($videoTerm) {
                $videoTerm->setTerm($term);
            }else {
                $videoTerm = new VideoTerm();
                $videoTerm->setVideo($entity);
                $videoTerm->setTerm($term);
            }
            $em->persist($videoTerm);
        }
        //处理课程
        $courseVideo = $em->getRepository('AppWebBundle:CourseVideo')->findOneByVideo($entity);
        if(!empty($request->get('course')['id']) && !empty($request->get('course_video')['chapter'])){
            $course = $em->getRepository('AppWebBundle:Course')->find($request->get('course')['id']);
            if($courseVideo && $course) {
                //更新
                $courseVideo->setCourse($course);
                $courseVideo->setDelta($request->get('course_video')['delta']);
                $courseVideo->setChapter($request->get('course_video')['chapter']);
                $em->persist($courseVideo);
            }elseif($course) {
                //新增
                $courseVideo = new CourseVideo();
                $courseVideo->setCourse($course);
                $courseVideo->setVideo($entity);
                $courseVideo->setChapter($request->get('course_video')['chapter']);
                $courseVideo->setDelta($request->get('course_video')['delta']);
                $em->persist($courseVideo);
            }
        }elseif(empty($request->get('course')['id']) && $courseVideo) {
            //删除
            $em->remove($courseVideo);
        }
        //处理知识点
        $snapshots = $em->getRepository('MediaVideoBundle:Snapshot')->findByVideo($entity);
        foreach($request->get('snapshot')['point'] as $k => $p) {
            if($p > 0 && !empty($request->get('snapshot')['summary'][$k])) {
                $snapshot = new Snapshot();
                $snapshot->setVideo($entity);
                $snapshot->setPoint(intval($p));
                $snapshot->setSummary($request->get('snapshot')['summary'][$k]);
                foreach($snapshots as $snst) {
                    if($p == $snst->getPoint()) {
                        $snst->setSummary($request->get('snapshot')['summary'][$k]);
                        $snapshot = $snst;
                    }
                }

                $em->persist($snapshot);
            }
        }
        foreach($snapshots as $snst) {
            if(!in_array($snst->getPoint(), $request->get('snapshot')['point'])) {
                $em->remove($snst);
            }
        }


        //检查表单验证结果
        if ($editForm->isValid()) {
            $em->flush();

            $request->getSession()->getFlashBag()->add(
                'success',
                '更新成功!'
            );
        }else {
            $errors = $editForm->getErrors();
            $current = $errors->current();
            $message = '<strong>更新失败!</strong>';
            if($current instanceof FormError){
                $message .= '<br /> 错误信息：' . $current->getMessage();
            }
            $request->getSession()->getFlashBag()->add(
                'failed',
                $message
            );
        }
        return $this->redirect($this->generateUrl('admin_video_edit', array('id' => $id)));
    }
    /**
     * Deletes a Video entity.
     *
     * @Route("/{id}", name="admin_video_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('MediaVideoBundle:Video')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Video entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_video'));
    }

    /**
     * Creates a form to delete a Video entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_video_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}

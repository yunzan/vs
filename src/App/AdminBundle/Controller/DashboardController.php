<?php

namespace App\AdminBundle\Controller;

use App\AdminBundle\Models\OSSVideoOptions;
use App\OSSBundle\Models\OSSOptions;
use App\OSSBundle\OSSClient;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

class DashboardController extends Controller
{
    const MAX_KEYS_LIMIT = 1000;
    /**
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {
        $video = array(); $course = array();
        $video['uploaded']['count'] = self::getObjectCountByBucket($this->container->getParameter(OSSVideoOptions::BUCKET));
        $video['uploaded']['size'] = self::getBucketSize($this->container->getParameter(OSSVideoOptions::BUCKET));
        //$video['uploaded']['duration'] = 0;
        //$video['uploaded']['count'] = 0;

        return array(
          'video' => $video,
          'course' => $course,
        );
    }

    protected function _getBasicConfig()
    {
        return array(
          OSSOptions::ENDPOINT => $this->container->getParameter(OSSVideoOptions::ENDPOINT),
          OSSOptions::ACCESS_KEY_ID => $this->container->getParameter(OSSVideoOptions::ACCESS_KEY_ID),
          OSSOptions::ACCESS_KEY_SECRET => $this->container->getParameter(OSSVideoOptions::ACCESS_KEY_SECRET),
        );
    }


    protected function getObjectCountByBucket($bucket = null)
    {
        $count = 0;
        $config = $this->_getBasicConfig();
        $OSS = OSSClient::factory($config);
        $marker = '';
        do {
            $res = $OSS->listObjects(array(
              OSSOptions::BUCKET => $bucket,
              OSSOptions::MAX_KEYS => self::MAX_KEYS_LIMIT,
              OSSOptions::MARKER => $marker,
            ));
            $count += count($res->getObjectSummarys());
        } while($marker = $res->getNextMarker());

        return $count;
    }

    protected function getBucketSize()
    {
        $size = 0;
        $config = $this->_getBasicConfig();
        $OSS = OSSClient::factory($config);
        $marker = '';
        do {
            $res = $OSS->listObjects(array(
              OSSOptions::BUCKET => $this->container->getParameter(OSSVideoOptions::BUCKET),
              OSSOptions::MARKER => $marker,
            ));

            $objects = $res->getObjectSummarys();
           foreach($objects as $obj) {
               $size += $obj->getSize();
           }
        } while($marker = $res->getNextMarker());

        return $size;
    }

}

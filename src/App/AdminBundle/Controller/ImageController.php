<?php

namespace App\AdminBundle\Controller;

use App\AdminBundle\Models\OSSImageOptions;
use App\OSSBundle\Models\OSSOptions;
use App\OSSBundle\OSSClient;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;

/**
 * @Route("/image")
 */
class ImageController extends Controller
{
    const MAX_KEYS = 20;
    protected function _getImageUploadConfig()
    {
        return array(
          OSSOptions::ENDPOINT => $this->container->getParameter(OSSImageOptions::ENDPOINT),
          OSSOptions::ACCESS_KEY_ID => $this->container->getParameter(OSSImageOptions::ACCESS_KEY_ID),
          OSSOptions::ACCESS_KEY_SECRET => $this->container->getParameter(OSSImageOptions::ACCESS_KEY_SECRET),
          OSSOptions::BUCKET => $this->container->getParameter(OSSImageOptions::BUCKET),
        );
    }
    /**
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {
        $res = self::getUploadedImage(array(OSSOptions::MAX_KEYS => self::MAX_KEYS));

        return array(
          'objects' => $res->getObjectSummarys(),
          'image_host' => OSSImageOptions::HOST,
          'marker' => $res->getNextMarker(),
        );
    }

    /**
     * @Route("/upload")
     * @Template()
     */
    public function uploadAction(Request $request)
    {


        if($request->isMethod(OSSImageOptions::UPLOAD_METHOD_POST)) {
            $handle = $request->files->get(OSSImageOptions::UPLOAD_PARAM);

            if($handle && $handle->isValid()) {
                $fileName = $request->request->get('name');
                if(!$fileName) {
                    $fileName = $handle->getClientOriginalName();
                }
                $handle->move(OSSImageOptions::UPLOAD_TMP_DIR . OSSImageOptions::SLASH, $fileName);

                echo 'success';
                die();
            }
            $count = $request->get('image-uploader_count');
            $type = $request->get('image-type');
            for($i =0; $i < $count; $i++) {
                if($fileName = $request->get('image-uploader_'.$i.'_name')){
                    $this->upload($fileName, $type);
                }
            }
            return $this->redirect($this->generateUrl('app_admin_image_index'));

        }

        return array(
                // ...
            );
    }

    protected function upload($fileName, $type)
    {
        $config = $this->_getImageUploadConfig();
        $OSS = OSSClient::factory($config);

        $filePath = OSSImageOptions::UPLOAD_TMP_DIR . OSSImageOptions::SLASH . $fileName;
        $file = new File($filePath);
        $OSS->putObject(array(
          OSSOptions::BUCKET => $config[OSSOptions::BUCKET],
          OSSOptions::KEY => $type . OSSImageOptions::SLASH . $fileName,
          OSSOptions::CONTENT => fopen($filePath, 'r'),
          OSSOptions::CONTENT_LENGTH => filesize($filePath),
          OSSOptions::CONTENT_TYPE => $file->getMimeType(),
          OSSOptions::CACHE_CONTROL => OSSImageOptions::CACHE_CONTROL,
          OSSOptions::EXPIRES => new \DateTime(OSSImageOptions::EXPIRE),
          OSSOptions::CONTENT_ENCODING => OSSImageOptions::CONTENT_ENCODING,
          OSSOptions::CONTENT_DISPOSITION => OSSImageOptions::CONTENT_DISPOSITION . 'filename=' . $fileName,
        ));
        unlink($filePath);
        return true;
    }

    /**
     * @Route("/scroll")
     * @Method("GET")
     */
    public function scrollAction()
    {
        $marker = $this->get('request')->get('marker');
        if(!$marker) {
            throw $this->createNotFoundException('Not found');
        }
        $res = self::getUploadedImage(array(
          OSSOptions::MAX_KEYS => self::MAX_KEYS,
          OSSOptions::MARKER => $marker,
        ));

        $vars = array(
          'objects' => $res->getObjectSummarys(),
          'image_host' => OSSImageOptions::HOST,
          'marker' => $res->getNextMarker(),
        );

        return $this->render('AppAdminBundle:Image:scroll.html.twig', $vars);
    }

    protected function getUploadedImage(array $options = null)
    {
        $config = $this->_getImageUploadConfig();
        $OSS = OSSClient::factory($config);

        return $OSS->listObjects(array_merge(array(OSSOptions::BUCKET => $config[OSSOptions::BUCKET]), $options));
    }

}

<?php

namespace App\AdminBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\SecurityContext;
use Symfony\Component\Security\Core\Exception\InvalidCsrfTokenException;
use Symfony\Component\Security\Core\Exception\DisabledException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\FormError;
use Symfony\Component\Security\Core\Exception\BadCredentialsException;
use App\AdminBundle\Form\Login;

class AuthController extends Controller
{
    /**
     * @Route("/login")
     * @Template()
     */
    public function loginAction(Request $request)
    {
        $tr = $this->get('translator');
        $form = $this->createForm(new Login(), null, array(
            'label' => array(
                'username' => $tr->trans('app.login.username', array(), 'admin'),
                'password' => $tr->trans('app.login.password', array(), 'admin'),
            )
        ));

        if ( $request->attributes->has(SecurityContext::AUTHENTICATION_ERROR) ) {
            $error = $request->attributes->get(SecurityContext::AUTHENTICATION_ERROR);
        } else {
            $error = $request->getSession()->get(SecurityContext::AUTHENTICATION_ERROR);
            $request->getSession()->set( SecurityContext::AUTHENTICATION_ERROR , null ) ;
        }



        if( $error ) {
            if( $error instanceof InvalidCsrfTokenException ) {
                $_error = new FormError( $tr->trans('app.login.error.crsf', array(), 'admin') ) ;
                $form->addError($_error);
            } else if( $error instanceof BadCredentialsException ) {
                $_error = new FormError( $tr->trans('app.login.error.credentials', array(), 'admin') ) ;
                $form->addError($_error);
            }  else if( $error instanceof DisabledException ) {
                $_error = new FormError( $tr->trans('app.login.error.disabled', array(), 'admin') ) ;
                $form->addError($_error);
            } else {
                $_error = new FormError( $error->getMessage() ) ;
                $form->addError($_error);
                if( $this->container->getParameter('kernel.debug') ) {
                    var_dump(  $error ) ;
                }
            }
        }
        $form->get('username')->setData($request->getSession()->get(SecurityContext::LAST_USERNAME));
        return array('form'=>$form->createView());
    }

    /**
     * @Route("/check")
     * @Template()
     */
    public function checkAction()
    {
        return array(
                // ...
            );
    }

    /**
     * @Route("/logout")
     * @Template()
     */
    public function logoutAction()
    {
        return array(
                // ...
            );
    }

}

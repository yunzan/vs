<?php

namespace App\AdminBundle\Controller;

use App\AdminBundle\Models\Veishu;
use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use App\WebBundle\Entity\Course;

/**
 * Category controller.
 *
 * @Route("/course")
 */
class CourseController extends Controller
{
    /**
     * List all courses
     *
     * @Route("/", name="admin_course")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $courses = $em->getRepository('AppWebBundle:Course')->findAll();

        return array(
            'courses' => $courses,
        );
    }

    /**
     * Edit Course
     *
     * @Route("/edit/{id}", name="admin_course_edit")
     * @Template()
     */
    public function editAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->get('course');

        $course = $em->getRepository('AppWebBundle:Course')->find($id);

        if(isset($data)){
            $course2 = $em->getRepository('AppWebBundle:Course')->findOneByNumber($data['number']);
            if($course2 && $course->getId() != $course2->getId()) {
                $request->getSession()->getFlashBag()->add(
                    'failed',
                    '编号重复！'
                );
            }elseif(!empty($data['number']) && !empty($data['title']) && !empty($data['slug']) && $course){
                $course->setNumber($data['number']);
                $course->setTitle($data['title']);
                $course->setDescription($data['description']);
                $course->setCover($data['cover']);
                $course->setSlug(Veishu::cleanSlug($data['slug']));
                $course->setCreatedValue();
                $course->setPosition($data['position']);

                $em->persist($course);
                $em->flush();

                $request->getSession()->getFlashBag()->add(
                    'success',
                    '更新成功！'
                );
            }else {
                $request->getSession()->getFlashBag()->add(
                    'failed',
                    '验证失败！'
                );
            }
        }

        return array(
            'course' => $course,
        );
    }

    /**
     * Add Course
     *
     * @Route("/add", name="admin_course_add")
     * @Template()
     */
    public function addAction(Request $request)
    {
        $em   = $this->getDoctrine()->getManager();
        $data = $request->get('course');

        if(!empty($data['number']) && !empty($data['title']) && !empty($data['slug'])) {
            $course = new Course();
            $course2 = $em->getRepository('AppWebBundle:Course')->findOneByNumber($data['number']);
            if($course2) {
                $request->getSession()->getFlashBag()->add(
                    'failed',
                    '编号重复！'
                );
                return;
            }
            $course->setNumber($data['number']);
            $course->setTitle($data['title']);
            $course->setDescription($data['description']);
            $course->setCover($data['cover']);
            $course->setCreatedValue();
            $course->setSlug(Veishu::cleanSlug($data['slug']));
            $course->setPosition($data['position']);

            $em->persist($course);
            $em->flush();

            $request->getSession()->getFlashBag()->add(
                'success',
                '创建成功！'
            );
        }elseif(isset($data)) {
            $request->getSession()->getFlashBag()->add(
                'failed',
                '验证失败！'
            );
        }

        return;
    }
}
<?php

namespace App\AdminBundle\Controller;

use App\AdminBundle\Models\OSSVideoOptions;
use App\OSSBundle\Models\OSSOptions;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use App\OSSBundle\OSSClient;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @Route("/video")
 */
class VideoController extends Controller
{
    const MAX_KEYS = 20;

    protected function _getVideoUploadConfig()
    {
        return array(
          OSSOptions::ENDPOINT => $this->container->getParameter(OSSVideoOptions::ENDPOINT),
          OSSOptions::ACCESS_KEY_ID => $this->container->getParameter(OSSVideoOptions::ACCESS_KEY_ID),
          OSSOptions::ACCESS_KEY_SECRET => $this->container->getParameter(OSSVideoOptions::ACCESS_KEY_SECRET),
          OSSOptions::BUCKET => $this->container->getParameter(OSSVideoOptions::BUCKET),
        );
    }
    /**
     * @Route("/")
     * @Template()
     */
    public function indexAction()
    {
        $res = self::getUploadedVideo(array(OSSOptions::MAX_KEYS => self::MAX_KEYS));

        return array(
          'objects' => $res->getObjectSummarys(),
          'video_host' => OSSVideoOptions::HOST,
          'marker' => $res->getNextMarker(),
        );
    }

    /**
     * @Route("/upload")
     * @Template()
     * 开发手册:http://aliyun_portal_storage.oss.aliyuncs.com/oss_api/oss_phphtml/index.html
     */
    public function uploadAction(Request $request)
    {
        $config = $this->_getVideoUploadConfig();
        $OSS = OSSClient::factory($config);

        if($request->isMethod(OSSVideoOptions::UPLOAD_METHOD_POST)) {
            $handle	= $request->files->get(OSSVideoOptions::UPLOAD_PARAM);

            if($handle && $handle->isValid()) {
                $fileName = $request->request->get('name');
                if(!$fileName) {
                    $fileName = $handle->getClientOriginalName();
                }
                $handle->move(OSSVideoOptions::UPLOAD_TMP_DIR . OSSVideoOptions::SLASH, $fileName);
                $filePath = OSSVideoOptions::UPLOAD_TMP_DIR . OSSVideoOptions::SLASH . $fileName;

                $OSS->putObject(array(
                  OSSOptions::BUCKET => $config[OSSOptions::BUCKET],
                  OSSOptions::KEY => $fileName,
                  OSSOptions::CONTENT => fopen($filePath, 'r'),
                  OSSOptions::CONTENT_LENGTH => $handle->getClientSize(),
                  OSSOptions::CONTENT_TYPE => $handle->getClientMimeType(),
                  OSSOptions::CACHE_CONTROL => OSSVideoOptions::CACHE_CONTROL,
                  OSSOptions::EXPIRES => new \DateTime(OSSVideoOptions::EXPIRE),
                  OSSOptions::CONTENT_ENCODING => OSSVideoOptions::CONTENT_ENCODING,
                ));
                unlink($filePath);
                echo 'success';die();
            }
            return $this->redirect($this->generateUrl('app_admin_video_index'));
        }

        return array(

        );
    }

    /**
     * @Route("/scroll")
     * @Method("GET")
     */
    public function scrollAction()
    {
        $marker = $this->get('request')->get('marker');
        if(!$marker) {
            throw $this->createNotFoundException('Not found');
        }
        $res = self::getUploadedVideo(array(
          OSSOptions::MAX_KEYS => self::MAX_KEYS,
          OSSOptions::MARKER => $marker,
        ));

        $vars = array(
          'objects' => $res->getObjectSummarys(),
          'video_host' => OSSVideoOptions::HOST,
          'marker' => $res->getNextMarker(),
        );

        return $this->render('AppAdminBundle:Video:scroll.html.twig', $vars);
    }

    protected function getUploadedVideo(array $options = null)
    {
        $config = $this->_getVideoUploadConfig();
        $OSS = OSSClient::factory($config);

        return $OSS->listObjects(array_merge(array(OSSOptions::BUCKET => $config[OSSOptions::BUCKET]), $options));
    }

}

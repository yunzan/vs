<?php

namespace App\AdminBundle\Controller;

use Symfony\Component\Form\FormError;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use App\CategoryBundle\Entity\Category;
use App\CategoryBundle\Entity\Term;
use App\AdminBundle\Models\Veishu;
/**
 * Category controller.
 *
 * @Route("/category")
 */
class CategoryController extends Controller
{
    /**
     * Lists all Category & Term entities.
     *
     * @Route("/", name="admin_category")
     * @Method("GET")
     * @Template()
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $categories = $em->getRepository('AppCategoryBundle:Category')->findBy(array(), array('position' => 'ASC'));
        $terms = $em->getRepository('AppCategoryBundle:Term')->findBy(array(), array('position' => 'ASC'));

        return array(
            'categories' => $categories,
            'terms' => $terms,
        );
    }

    /**
     * Add Category
     *
     * @Route("/add", name="admin_category_add")
     * @Method("POST")
     */
    public function addCategoryAction(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $data = $request->get('add_category');
        if(!empty($data['name']) && !empty($data['slug'])) {
            $category = new Category();

            $category->setName($data['name']);
            $category->setSlug(Veishu::cleanSlug($data['slug']));
            $category->setPosition(is_numeric($data['position'])?$data['position']:0);

            $em->persist($category);
            $em->flush();
            $request->getSession()->getFlashBag()->add(
                'success',
                '添加成功!'
            );
        } else {
            $request->getSession()->getFlashBag()->add(
                'failed',
                '验证失败!'
            );
        }

        return $this->redirect($this->generateUrl('admin_category'));
    }

    /**
     * Add Term
     *
     * @Route("/term/add", name="admin_term_add")
     * @Method("POST")
     */
    public function addTerm(Request $request)
    {
        $em = $this->getDoctrine()->getManager();

        $data = $request->get('add_term');
        $category = $em->getRepository('AppCategoryBundle:Category')->find($data['category']);
        if(!empty($data['name']) && !empty($data['slug']) && $category) {
            $term = new Term();

            $term->setCategory($category);
            $term->setName($data['name']);
            $term->setSlug(Veishu::cleanSlug($data['slug']));
            $term->setPosition(is_numeric($data['position'])?$data['position']:0);

            $em->persist($term);
            $em->flush();
            $request->getSession()->getFlashBag()->add(
                'success',
                '创建成功!'
            );
        } else {
            $request->getSession()->getFlashBag()->add(
                'failed',
                '验证失败!'
            );
        }

        return $this->redirect($this->generateUrl('admin_category'));
    }

    /**
     * Edit Category
     *
     * @Route("/edit/{id}", name="admin_category_edit")
     * @Template()
     */
    public function editAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $category = $em->getRepository('AppCategoryBundle:Category')->find($id);
        $data = $request->get('category');

        if(isset($data)) {
            if(!empty($data['name']) && !empty($data['slug']) && $category) {
                $category->setName($data['name']);
                $category->setSlug(Veishu::cleanSlug($data['slug']));
                $category->setPosition(is_numeric($data['position'])?$data['position']:0);

                $em->persist($category);
                $em->flush();

                $request->getSession()->getFlashBag()->add(
                    'success',
                    '更新成功!'
                );
            } else {
                $request->getSession()->getFlashBag()->add(
                    'failed',
                    '验证失败!'
                );
            }

            return $this->redirect($this->generateUrl('admin_category'));
        }

        return array(
            'category' => $category,
        );
    }

    /**
     * Edit Term
     *
     * @Route("/edit/term/{id}", name="admin_term_edit")
     * @Template()
     */
    public function editTermAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();
        $data = $request->get('term');

        $term = $em->getRepository('AppCategoryBundle:Term')->find($id);
        $categories = $em->getRepository('AppCategoryBundle:Category')->findAll();

        if(isset($data)) {
            $category = $em->getRepository('AppCategoryBundle:Category')->find($data['category']);
            if(!empty($data['name']) && !empty($data['slug']) && $category && $term) {

                $term->setCategory($category);
                $term->setName($data['name']);
                $term->setSlug(Veishu::cleanSlug($data['slug']));
                $term->setPosition(is_numeric($data['position'])?$data['position']:0);

                $em->persist($term);
                $em->flush();

                $request->getSession()->getFlashBag()->add(
                    'success',
                    '更新成功!'
                );
            } elseif(isset($data)) {
                $request->getSession()->getFlashBag()->add(
                    'failed',
                    '验证失败!'
                );
            }

            return $this->redirect($this->generateUrl('admin_category'));
        }

        return array(
            'categories' => $categories,
            'term'       => $term,
        );
    }
}

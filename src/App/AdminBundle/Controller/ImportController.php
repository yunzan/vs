<?php

namespace App\AdminBundle\Controller;

use App\AdminBundle\Form\Import;
use App\AdminBundle\Models\ImportMap;
use App\WebBundle\Entity\Course;
use App\WebBundle\Entity\CourseVideo;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Form\FormError;
use Media\VideoBundle\Entity\Video;
use Media\VideoBundle\Entity\VideoTerm;
use Media\VideoBundle\Entity\Snapshot;
use App\CategoryBundle\Entity\Category;
use App\CategoryBundle\Entity\Hierarchy;
use App\CategoryBundle\Entity\Term;


/**
 * @Route("/import")
 */
class ImportController extends Controller
{
    const BASE_DATA = '2015-01-01';
    const BASE_TIME = '00:00:00';
    const VIDEO = 'video';
    const TERM = 'term';
    const CATEGORY = 'category';
    const COURSE = 'course';
    const PARENT = 'parent';

    private $videos;
    private $snapshots;
    private $videoterms;
    private $categories;
    private $terms;
    private $hierarchies;
    private $courses;
    private $coursevideos;

    /**
     * @Route("/video")
     * @Template()
     */
    public function videoAction(Request $request)
    {
        $tr = $this->get('translator');

        $form = $this->createForm(new Import(), null, array(
          'label' => array(
            'file' => $tr->trans('app.form.file.label', array(), 'admin')
          )
        ));

        if($request->isMethod('POST')) {
            $form->submit($request);

            if($form->isValid()){
                $handel	= $request->files->get('import')['file'];
                $res = array('status'=>1);

                if(!$handel->isValid() ) {
                    $res['status'] = 0;
                    $res['message'] = 'upload invalid';
                }else {
                    $ext	= strtolower( pathinfo( $handel->getClientOriginalName() , PATHINFO_EXTENSION) ) ;
                    if( !in_array($ext, array('csv')) ) {
                        $res['status'] = 0;
                        $res['message'] = sprintf( 'only allow (%s) you upload %s', join(', ', array('csv') ), $ext  );
                    }
                    elseif( $handel->getClientSize()  > 1000000 ) {
                        $res['status'] = 0;
                        $res['message'] = sprintf( 'size %s bigger then %s ', $handel->getClientSize(), 1000000  );
                    }
                }

                if($res['status']) {
                    $stream = fopen($handel->getPathname(), 'rb');
                    while($row = fgetcsv($stream)) {
                        $res['data'][] = $row;
                    }

                    $preData = self::parseData($res['data']);

                    if(self::validateData($preData)) {
                        self::saveData($preData);
                    }
                    //return  new JsonResponse($res);
                }
            }
            else{
                return  JsonResponse( array(
                  'invalid request!' ,
                ) );
            }

        }

        if(isset($res['message'])){
            $form->addError(new FormError($res['message']));
        }

        return array(
            'form' => $form->createView()
        );
    }

    /**
     * @param array $data
     * @return array
     * 解析数据
     */
    protected function parseData(array $data)
    {
        $header = $data[0];
        //正式数据从第三行开始
        unset($data[0]);//第一行为为表头
        unset($data[1]);//第二行为字段描述

        $preData = array();
        foreach($data as $item) {
            $values = array();
            foreach($header as $k => $v) {
                if(isset($values[$v])) {
                    if(!is_array($values[$v])) {
                        $values[$v] = array($values[$v]);
                    }
                    $values[$v][] = $item[$k];
                }else {
                    $values[$v] = $item[$k];
                }

            }
            $preData[] = $values;
        }
        return $preData;
    }

    /**
     * @param array $data
     * @return bool
     * Todo: validator
     */
    protected function validateData(array $data)
    {
        return true;
    }

    protected function saveData(array $data)
    {
        foreach($data as $key => $item) {
            $video = new Video();
            $category = new Category();
            $term = new Term();
            $videoTerm = new VideoTerm();
            $hierarchy = new Hierarchy();
            $course = new Course();
            $courseVideo = new CourseVideo();

            //填充视频数据
            $video->setNumber($item[ImportMap::NUMBER]);
            $video->setTitle($item[ImportMap::TITLE]);
            $video->setCover($item[ImportMap::COVER]);
            $video->setSource($item[ImportMap::SOURCE]);
            $video->setDescription($item[ImportMap::DESCRIPTION]);
            $video->setDuration(self::parseTime($item[ImportMap::DURATION]));
            $video->setFormat($item[ImportMap::FORMAT]);
            $video->setCreated(new \DateTime());
            $video->setAllowSale($item[ImportMap::ALLOW_SALE]);
            $video->setSize(intval($item[ImportMap::SIZE]));
            $video->setSpeakerTitle($item[ImportMap::SPEAKER_TITLE]);
            $video->setSpeaker($item[ImportMap::SPEAKER]);
            $video->setSpeakerIntro($item[ImportMap::SPEAKER_INTRO]);
            $video->setSpeakerAvatar($item[ImportMap::SPEAKER_AVATAR]);
            $video->setSlug($item[ImportMap::SLUG]);

            $video = self::createVideo($video);

            //填充分类
            if(isset($item[ImportMap::TERM][0])) {
                $category->setName($item[ImportMap::TERM][0]);
                $category->setSlug($item[ImportMap::TERM_SLUG][0]);

                $category = self::createCategory($category);
            }


            //填充子分类
            if(isset($item[ImportMap::TERM][1])) {
                $term->setName($item[ImportMap::TERM][1]);
                $term->setSlug($item[ImportMap::TERM_SLUG][1]);
                $term->setCategory($category);

                $term = self::createTerm($term);
            }


            //填充分类对应关系
            $hierarchy->setTerm($term);
            $hierarchy->setParent(null);

            self::createHierarchy($hierarchy);

            //填充视频分类对应
            $videoTerm->setTerm($term);
            $videoTerm->setVideo($video);

            self::createVideoTerm($videoTerm);

            //填充知识点
            foreach($item[ImportMap::POINT] as $k => $p) {
                if($p) {
                    $snapshot = new Snapshot();

                    $snapshot->setVideo($video);
                    $snapshot->setPoint(self::parseTime($p));
                    $snapshot->setSummary($item[ImportMap::SUMMARY][$k]);

                    self::createSnapshot($snapshot);
                }
            }

            //填充课程
            if($item[ImportMap::COURSE_NUMBER]) {
                $course->setNumber($item[ImportMap::COURSE_NUMBER]);
                $course->setTitle($item[ImportMap::COURSE_TITLE]);
                $course->setCover($item[ImportMap::COURSE_COVER]);
                $course->setSlug($item[ImportMap::COURSE_SLUG]);
                $course->setDescription($item[ImportMap::COURSE_DESC]);
                $course->setCreated(new \DateTime());

                $course = self::createCourse($course);

                //填充课程视频关系
                $courseVideo->setVideo($video);
                $courseVideo->setChapter($item[ImportMap::COURSE_CHAPTER]);
                $courseVideo->setCourse($course);
                $courseVideo->setDelta($item[ImportMap::DELTA_IN_COURSE]);

                self::createCourseVideo($courseVideo);
            }
        }
        $this->getDoctrine()->getManager()->flush();
    }

    protected function createVideo(Video $video)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('MediaVideoBundle:Video')->findOneBy(array(ImportMap::NUMBER => $video->getNumber()));
        if($entity) {
            return $entity;
        }
        if(isset($this->videos[$video->getNumber()])) {
            return $this->videos[$video->getNumber()];
        }
        $this->videos[$video->getNumber()] = $video;
        $em->persist($video);
        //$em->flush();
        return $video;

    }

    protected function createSnapshot(Snapshot $snapshot)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('MediaVideoBundle:Snapshot')->findOneBy(array(
          ImportMap::POINT => $snapshot->getPoint(),
          self::VIDEO => $snapshot->getVideo(),
        ));
        if($entity) {
            return $entity;
        }
        if(isset($this->snapshots[base64_encode($snapshot->getPoint() . $snapshot->getVideo()->getNumber())])) {
            return $this->snapshots[base64_encode($snapshot->getPoint() . $snapshot->getVideo()->getNumber())];
        }
        $this->snapshots[base64_encode($snapshot->getPoint() . $snapshot->getVideo()->getNumber())] = $snapshot;
        $em->persist($snapshot);
        //$em->flush();
        return $snapshot;
    }

    protected function createVideoTerm(VideoTerm $videoTerm)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('MediaVideoBundle:VideoTerm')->findOneBy(array(
          self::VIDEO => $videoTerm->getVideo(),
        ));
        if($entity) {
            return $entity;
        }
        if(isset($this->videoterms[$videoTerm->getVideo()->getNumber()])) {
            return $this->videoterms[$videoTerm->getVideo()->getNumber()];
        }
        $this->videoterms[$videoTerm->getVideo()->getNumber()] = $videoTerm;
        $em->persist($videoTerm);
        //$em->flush();
        return $videoTerm;

    }

    protected function createCategory(Category $category)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppCategoryBundle:Category')->findOneBy(array(
          ImportMap::NAME => $category->getName(),
        ));
        if($entity) {
            return $entity;
        }
        if(isset($this->categories[base64_encode($category->getName())])) {
            return $this->categories[base64_encode($category->getName())];
        }
        $this->categories[base64_encode($category->getName())] = $category;
        $em->persist($category);
        //$em->flush();
        return $category;

    }

    protected function createTerm(Term $term)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppCategoryBundle:Term')->findOneBy(array(
          ImportMap::NAME => $term->getName(),
          self::CATEGORY => $term->getCategory(),
        ));
        if($entity) {
            return $entity;
        }
        if(isset($this->terms[base64_encode($term->getName())])) {
            return $this->terms[base64_encode($term->getName())];
        }
        $this->terms[base64_encode($term->getName())] = $term;
        $em->persist($term);
        //$em->flush();
        return $term;

    }

    protected function createHierarchy(Hierarchy $hierarchy)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppCategoryBundle:Hierarchy')->findOneBy(array(
          self::TERM => $hierarchy->getTerm(),
          self::PARENT => $hierarchy->getParent(),
        ));
        if($entity) {
            return $entity;
        }
        if(isset($this->hierarchies[base64_encode($hierarchy->getTerm()->getName())])) {
            return $this->hierarchies[base64_encode($hierarchy->getTerm()->getName())];
        }
        $this->hierarchies[base64_encode($hierarchy->getTerm()->getName())] = $hierarchy;
        $em->persist($hierarchy);
        //$em->flush();
        return $hierarchy;

    }

    protected function createCourse(Course $course)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppWebBundle:Course')->findOneBy(array(
          ImportMap::NUMBER => $course->getNumber(),
        ));
        if($entity) {
            return $entity;
        }
        if(isset($this->courses[$course->getNumber()])) {
            return $this->courses[$course->getNumber()];
        }
        $this->courses[$course->getNumber()] = $course;
        $em->persist($course);
        //$em->flush();
        return $course;

    }

    protected function createCourseVideo(CourseVideo $courseVideo)
    {
        $em = $this->getDoctrine()->getManager();
        $entity = $em->getRepository('AppWebBundle:CourseVideo')->findOneBy(array(
          self::COURSE => $courseVideo->getCourse(),
          self::VIDEO => $courseVideo->getVideo(),
        ));
        if($entity) {
            return $entity;
        }
        if(isset($this->coursevideos[$courseVideo->getCourse()->getNumber() .'-'.$courseVideo->getVideo()->getNumber()])) {
            return $this->coursevideos[$courseVideo->getCourse()->getNumber() .'-'.$courseVideo->getVideo()->getNumber()];
        }
        $this->coursevideos[$courseVideo->getCourse()->getNumber() .'-'.$courseVideo->getVideo()->getNumber()] = $courseVideo;
        $em->persist($courseVideo);
        //$em->flush();
        return $courseVideo;

    }

    protected function parseTime($p)
    {
        return strtotime(self::BASE_DATA . ' ' . $p) - strtotime(self::BASE_DATA . ' ' . self::BASE_TIME);
    }

}

/**
 * Created by jeremy on 14-12-31.
 */
$(function() {
    if(typeof Scrolload == 'undefined') {
        return false;
    }
    $(window).scrollTop($(window).scrollTop()-1);
    $(window).scroll(function()
    {
        if(typeof Scrolload.end != 'undefined' && Scrolload.end) return false;
        if(typeof Scrolload.url != 'undefined' && Scrolload.url && $(window).scrollTop() == $(document).height() - $(window).height())
        {
            if(Scrolload.lock) return false;
            Scrolload.lock = true;
            Scrolload.loading.appendTo(Scrolload.wrapper);
            setTimeout(function(){Scrolload.lock = false}, 500);
            $.ajax({
                url: Scrolload.url,
                success: function(html)
                {
                    Scrolload.loading.remove();
                    if(html)
                    {
                        Scrolload.wrapper.append(html);
                    }
                    Scrolload.url = Scrolload.wrapper.children().last().attr('next-url');
                },
                error: function() {
                    Scrolload.loading.remove();
                    Scrolload.end = true;
                }
            });
        }
    });
})
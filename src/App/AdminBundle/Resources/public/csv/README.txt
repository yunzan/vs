**导入说明**
文件import.sample.csv为导入的示例文档。

1. 请使用Excel打开后另存为后开始添加内容。注意：新保存的文件名尽量避免使用中文和特殊字符。
2. 文档第一行为程序标识行，不得随意修改，只有知识点的三个字段可以进行复制添加列。
3. 文档第二行为标识字段的说明文字，请保留，程序会跳过导入该行。
4. 目前视频暂定为两级分类，第一个term为一级分类，第二个term为二级分类。
5. 知识点有2个字段，point\summary，示例文档里预置了两个知识点，需要加更多知识点，需要将第一行的point\summary 复制一次添加在后面
   如果有3个知识点，那么表头列末应该识 point|summary|point|summary|point|summary 以此类推。
6. 如果使用文本编辑器修改csv文件，请注意给内容加上双引号，尤其是含有英文逗号单个字段内容必须用双引号包起来。

**字段说明**
number
编号
title
标题
format
格式
duration
时长
cover
封面图片
description
视频描述
speaker
主讲人
speakerTitle
主讲人职称
speakerAvatar
主讲人头像
speakerIntro
主讲人简介
size
文件大小
caseNumber
课程编号
caseTitle
课程标题
caseDesc
课程描述
caseChapter
课程章节
deltaInCase
课程内序列
allowSale
是否可售
term
一级分类
term
二级分类
point
知识点1
summary
知识点1概要
point
知识点2
summary
知识点2概要
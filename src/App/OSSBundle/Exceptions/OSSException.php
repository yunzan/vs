<?php
/**
 * Copyright (C) Alibaba Cloud Computing
 * All rights reserved.
 *
 * 版权所有 （C）阿里云计算有限公司
 */
namespace App\OSSBundle\Exceptions;

use App\CommonBundle\Exceptions\ServiceException;

/**
 * 该异常在对开放存储数据服务（Open Storage Service）访问失败时抛出。
 *
 * @package App\OSSBundle\Exceptions
 */
class OSSException extends ServiceException {
    public function __construct($code, $message, $requestId, $hostId) {
        parent::__construct($code, $message, $requestId, $hostId);
    }
}

<?php
/**
 * Copyright (C) Alibaba Cloud Computing
 * All rights reserved.
 *
 * 版权所有 （C）阿里云计算有限公司
 */
namespace App\OSSBundle\Parsers\SXParser;

use App\CommonBundle\Utilities\DateUtils;

use App\CommonBundle\Communication\HttpResponse;

use App\OSSBundle\Models\OSSObject;

use App\OSSBundle\Utilities\OSSHeaders;

use App\OSSBundle\Models\OSSOptions;

use App\OSSBundle\Utilities\OSSUtils;

class SXGetObjectParser extends SXParser {

    public function parse(HttpResponse $response, $options) {
        $object = new OSSObject();
        $object->setBucketName($options[OSSOptions::BUCKET]);
        $object->setKey($options[OSSOptions::KEY]);

        if (!$options[OSSOptions::META_ONLY])
            $object->setObjectContent($response->getContent());

        foreach ($response->getHeaders() as $key => $value) {
            if ($key == OSSHeaders::LAST_MODIFIED) {
                $object->addMetadata(OSSHeaders::LAST_MODIFIED, DateUtils::parseDate($value));
            } else if ($key == OSSHeaders::CONTENT_LENGTH) {
                $object->addMetadata(OSSHeaders::CONTENT_LENGTH, (int) $value);
            } else if ($key == OSSHeaders::ETAG) {
                $object->addMetadata(OSSHeaders::ETAG, OSSUtils::trimQuotes($value));
            } else if (strpos($key, OSSHeaders::OSS_USER_META_PREFIX) === 0) {
                $key = substr($key, strlen(OSSHeaders::OSS_USER_META_PREFIX));
                $object->addUserMetadata($key, $value);
            } else {
                $object->addMetadata($key, $value);
            }
        }
        return $object;
    }
}

<?php
/**
 * Copyright (C) Alibaba Cloud Computing
 * All rights reserved.
 *
 * 版权所有 （C）阿里云计算有限公司
 */
namespace App\OSSBundle\Parsers\SXParser;


use App\CommonBundle\Communication\ResponseParserInterface;

use App\CommonBundle\Communication\HttpResponse;

/**
 * Class EmptyParser
 * do nothing for the service that need not be parsed.
 * @package App\OSSBundle\Parsers\SXParser
 */
class SXEmptyParser implements  ResponseParserInterface {
    public function parse(HttpResponse $response, $options) {
        // Do nothing...
    }
}
<?php
/**
 * Copyright (C) Alibaba Cloud Computing
 * All rights reserved.
 *
 * 版权所有 （C）阿里云计算有限公司
 */
namespace App\OSSBundle\Parsers\SXParser;

use App\CommonBundle\Communication\HttpResponse;

use App\OSSBundle\Models\CompleteMultipartUploadResult;

use App\OSSBundle\Utilities\OSSUtils;

class SXCompleteMultipartUploadParser extends SXParser {

    public function parse(HttpResponse $response, $options) {
        $xml = $this->getXmlObject($response->getContent());
        $result = new CompleteMultipartUploadResult();
        $result->setETag(OSSUtils::trimQuotes((string) $xml->ETag));
        $result->setLocation((string) $xml->Location);
        $result->setBucketName((string) $xml->Bucket);
        $result->setKey((string) $xml->Key);

        return $result;
    }
}
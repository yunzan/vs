<?php
/**
 * Copyright (C) Alibaba Cloud Computing
 * All rights reserved.
 *
 * 版权所有 （C）阿里云计算有限公司
 */
namespace App\OSSBundle\Parsers\SXParser;

use App\OSSBundle\Models\PutObjectResult;

use App\CommonBundle\Communication\HttpResponse;

use App\OSSBundle\Utilities\OSSHeaders;

use App\CommonBundle\Communication\ResponseParserInterface;

use App\OSSBundle\Utilities\OSSUtils;


class SXPutObjectParser implements ResponseParserInterface {

    public function parse(HttpResponse $response, $options) {
        $putObjectResult = new PutObjectResult();
        $putObjectResult->setETag(OSSUtils::trimQuotes($response->getHeader(OSSHeaders::ETAG)));
        return $putObjectResult;
    }
}
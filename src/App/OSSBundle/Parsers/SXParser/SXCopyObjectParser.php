<?php
/**
 * Copyright (C) Alibaba Cloud Computing
 * All rights reserved.
 *
 * 版权所有 （C）阿里云计算有限公司
 */
namespace App\OSSBundle\Parsers\SXParser;

use App\CommonBundle\Communication\HttpResponse;

use App\CommonBundle\Utilities\DateUtils;

use App\OSSBundle\Models\CopyObjectResult;

use App\OSSBundle\Utilities\OSSUtils;

class SXCopyObjectParser extends SXParser {

    public function parse(HttpResponse $response, $options) {
        $xml = $this->getXmlObject($response->getContent());
        $lastModified = DateUtils::parseDate((string) $xml->LastModified);
        $eTag = OSSUtils::trimQuotes((string) $xml->ETag);

        $copyObjectResult = new CopyObjectResult();

        $copyObjectResult->setLastModified($lastModified);
        $copyObjectResult->setETag($eTag);
        return $copyObjectResult;
    }
}
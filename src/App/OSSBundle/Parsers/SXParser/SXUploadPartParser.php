<?php
/**
 * Copyright (C) Alibaba Cloud Computing
 * All rights reserved.
 *
 * 版权所有 （C）阿里云计算有限公司
 */
namespace App\OSSBundle\Parsers\SXParser;

use App\CommonBundle\Communication\HttpResponse;

use App\OSSBundle\Models\UploadPartResult;

use App\OSSBundle\Utilities\OSSHeaders;

use App\CommonBundle\Communication\ResponseParserInterface;

use App\OSSBundle\Models\OSSOptions;

use App\OSSBundle\Utilities\OSSUtils;

class SXUploadPartParser implements ResponseParserInterface {

    public function parse(HttpResponse $response, $options) {
        $result = new UploadPartResult();
        $result->setETag(OSSUtils::trimQuotes($response->getHeader(OSSHeaders::ETAG)));
        $result->setPartNumber($options[OSSOptions::PART_NUMBER]);
        return $result;
    }
}
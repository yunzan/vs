<?php
/**
 * Copyright (C) Alibaba Cloud Computing
 * All rights reserved.
 *
 * 版权所有 （C）阿里云计算有限公司
 */
namespace App\OSSBundle\Parsers\SXParser;

use App\CommonBundle\Communication\HttpResponse;

use App\OSSBundle\Models\Bucket;

use App\OSSBundle\Models\OSSOptions;

class SXCreateBucketParser extends SXParser {

    public function parse(HttpResponse $response, $options) {
        return new Bucket($options[OSSOptions::BUCKET]);
    }
}
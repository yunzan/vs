<?php
/**
 * Copyright (C) Alibaba Cloud Computing
 * All rights reserved.
 *
 * 版权所有 （C）阿里云计算有限公司
 */
namespace App\OSSBundle\ResponseHandlers;

use App\OSSBundle\Utilities\OSSResponseParserFactory;

use App\OSSBundle\Utilities\OSSExceptionFactory;

use App\CommonBundle\Communication\HttpResponse;

use App\CommonBundle\Communication\ResponseHandlerInterface;

class OSSErrorResponseHandler implements ResponseHandlerInterface {
    public function handle(HttpResponse $response) {
        if ($response->isSuccess()) {
            return;
        }
        
        if (!$response->getContent() || $response->getContentLength() <= 0) {
            throw OSSExceptionFactory::factory()->createInvalidResponseException('ServerReturnsUnknownError');
        }
        
        $error = OSSResponseParserFactory::factory()->createErrorParser()->parse($response, null);
        throw OSSExceptionFactory::factory()->createFromError($error);
    }
}

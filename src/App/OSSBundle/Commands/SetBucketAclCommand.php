<?php
/**
 * Copyright (C) Alibaba Cloud Computing
 * All rights reserved.
 *
 * 版权所有 （C）阿里云计算有限公司
 */
namespace App\OSSBundle\Commands;

use App\CommonBundle\Utilities\AssertUtils;

use App\CommonBundle\Utilities\HttpMethods;

use App\OSSBundle\Utilities\OSSHeaders;

use App\OSSBundle\Models\OSSOptions;

use App\OSSBundle\Utilities\OSSRequestBuilder;

use App\OSSBundle\Utilities\OSSUtils;


class SetBucketAclCommand extends OSSCommand {

    protected function checkOptions($options) {
        $options = parent::checkOptions($options);
        AssertUtils::assertSet(array(
            OSSOptions::BUCKET,
            OSSOptions::ACL
        ), $options);

        if (isset($options[OSSOptions::KEY])) {
            unset($options[OSSOptions::KEY]);
        }

        OSSUtils::assertBucketName($options[OSSOptions::BUCKET]);

        return $options;
    }

    protected function getRequest($options) {
        return OSSRequestBuilder::factory()
            ->setEndpoint($options[OSSOptions::ENDPOINT])
            ->setBucket($options[OSSOptions::BUCKET])
            ->addParameter('acl', null)
            ->addHeader(OSSHeaders::OSS_CANNED_ACL, $options[OSSOptions::ACL])
            ->setMethod(HttpMethods::PUT)
            ->build();
    }
}

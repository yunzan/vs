<?php
namespace App\OSSBundle\Commands;
/**
 * Copyright (C) Alibaba Cloud Computing
 * All rights reserved.
 *
 * 版权所有 （C）阿里云计算有限公司
 */
use App\CommonBundle\Utilities\HttpMethods;

use App\OSSBundle\Parsers\OSSResponseParserFactory;

use App\OSSBundle\Parsers\ListBucketParser;

use App\OSSBundle\Models\OSSOptions;

use App\OSSBundle\Utilities\OSSRequestBuilder;

use App\OSSBundle\Utilities\OSSUtils;


class ListBucketsCommand extends OSSCommand {
    protected function checkOptions($options) {
        $options = parent::checkOptions($options);
        if (isset($options[OSSOptions::BUCKET])) {
            unset($options[OSSOptions::BUCKET]);
        }

        if (isset($options[OSSOptions::KEY])) {
            unset($options[OSSOptions::KEY]);
        }

        return $options;
    }

    protected function getRequest($options) {
        return OSSRequestBuilder::factory()
                        ->setEndpoint($options[OSSOptions::ENDPOINT])
                        ->setMethod(HttpMethods::GET)
                        ->build();
    }
}

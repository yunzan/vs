<?php
/**
 * Copyright (C) Alibaba Cloud Computing
 * All rights reserved.
 *
 * 版权所有 （C）阿里云计算有限公司
 */
namespace App\OSSBundle\Commands;

use App\CommonBundle\Utilities\HttpMethods;

use App\OSSBundle\Utilities\OSSHeaders;

use App\OSSBundle\Models\OSSOptions;

use App\CommonBundle\Utilities\AssertUtils;

use App\OSSBundle\Utilities\OSSRequestBuilder;

use App\OSSBundle\Utilities\OSSUtils;

class InitiateMultipartUploadCommand extends OSSCommand {

    protected function checkOptions($options) {
        $options = parent::checkOptions($options);
        AssertUtils::assertSet(array(
            OSSOptions::BUCKET,
            OSSOptions::KEY,
        ), $options);

        OSSUtils::assertBucketName($options[OSSOptions::BUCKET]);
        OSSUtils::assertObjectKey($options[OSSOptions::KEY]);

        return $options;
    }

    protected function commandOptions() {
        return array(
            OSSOptions::CONTENT_TYPE => OSSUtils::DEFAULT_CONTENT_TYPE,
        );
    }

    protected function getRequest($options) {

        return OSSRequestBuilder::factory()
            ->addObjectMetadataHeaders($options)
            ->setEndpoint($options[OSSOptions::ENDPOINT])
            ->setMethod(HttpMethods::POST)
            ->setBucket($options[OSSOptions::BUCKET])
            ->setKey($options[OSSOptions::KEY])
            ->addParameter(OSSUtils::SUBRESOURCE_UPLOADS, null)
            ->setContent('')
            ->build();
    }
}
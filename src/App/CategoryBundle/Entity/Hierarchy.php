<?php

namespace App\CategoryBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Hierarchy
 *
 * @ORM\Table(name="app__hierarchy")
 * @ORM\Entity
 */
class Hierarchy
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\JoinColumn(name="term_id", referencedColumnName="id", unique=true)
     * @ORM\ManyToOne(targetEntity="App\CategoryBundle\Entity\Term")
     */
    private $term;

    /**
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     * @ORM\ManyToOne(targetEntity="App\CategoryBundle\Entity\Term")
     */
    private $parent;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set term
     *
     * @param \App\CategoryBundle\Entity\Term $term
     * @return Hierarchy
     */
    public function setTerm(\App\CategoryBundle\Entity\Term $term = null)
    {
        $this->term = $term;

        return $this;
    }

    /**
     * Get term
     *
     * @return \App\CategoryBundle\Entity\Term 
     */
    public function getTerm()
    {
        return $this->term;
    }

    /**
     * Set parent
     *
     * @param \App\CategoryBundle\Entity\Term $parent
     * @return Hierarchy
     */
    public function setParent(\App\CategoryBundle\Entity\Term $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return \App\CategoryBundle\Entity\Term 
     */
    public function getParent()
    {
        return $this->parent;
    }

}

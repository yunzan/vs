<?php

namespace App\CategoryBundle\Entity;

use Doctrine\ORM\EntityRepository;
use Media\VideoBundle\Models\Video as V;

class CategoryRepository extends EntityRepository
{

    private static $findVideoByTermQuery = 'SELECT v FROM MediaVideoBundle:Video v
                JOIN MediaVideoBundle:VideoTerm vt
                WHERE vt.video = v.id
                WHERE vt.term = :term
                AND v.status = :status
                AND v.onlyInCourse = :onlyin
                ORDER BY v.position ASC';

    public function findAllVideoByCategory(\App\CategoryBundle\Entity\Category $category, $limit = 10, $offset = 0)
    {
        $query = $this->getEntityManager()
          ->createQuery(
                self::$findVideoByTermQuery
          )
          ->setFirstResult($offset)
          ->setMaxResults($limit)
          ->setParameter('cid', $category->getId())
          ->setParameter('status', V::STATUS_PUBLISHED)
          ->setParameter('onlyin', 0);

        $videos = array();
        try {
            foreach($query->getResult() as $res) {
                $videos[] = $res->getVideo();
            }
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }

        return $videos;
    }

    public function findAllVideoByTerm(\App\CategoryBundle\Entity\Term $term, $limit = 10, $offset = 0)
    {
        $query = $this->getEntityManager()
            ->createQuery(
                self::$findVideoByTermQuery
            )
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->setParameter('term', $term->getId())
            ->setParameter('status', V::STATUS_PUBLISHED)
            ->setParameter('onlyin', 0);

        try {
            $videos = $query->getResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }

        return $videos;
    }

    public function countVideoByTerm(\App\CategoryBundle\Entity\Term $term)
    {
        $query = $this->getEntityManager()
            ->createQuery(
                $this->findVideoByTermCountQuery()
            )
            ->setParameter('term', $term->getId())
            ->setParameter('status', V::STATUS_PUBLISHED)
            ->setParameter('onlyin', 0);

        try {
            $count = $query->getSingleScalarResult();
        } catch (\Doctrine\ORM\NoResultException $e) {
            return null;
        }

        return $count;
    }

    private function findVideoByTermCountQuery()
    {
        return str_replace('SELECT v FROM', 'SELECT COUNT(v) FROM', self::$findVideoByTermQuery);
    }




}